<?php
class WBPanelController extends WB_Controller{

	public $model;
	public $composer;


	public function __construct(){
		parent::__construct();
		$this->composer = new WB_Composer( 'template/wb-panel/skel.php' );
		
        $this->composer->extend( 'root/head', 'template/wb-panel/shell_head.php');
        //$this->composer->extend( 'root/head/app-bar', 'template/wb-panel/app-bar.php');
		$this->composer->extend( 'root/app-bar', 'template/wb-panel/app-bar.php');
		//$this->composer->extend('root/xray', 'view/wb-panel/xray/wb-xray.php');


	}


	public function index(){
		$this->composer->setData('PageTitle', 'WB-Panel');

		
		$this->composer->extend('root/body', 'view/wb-panel/index.php');

		
		$response = $this->composer->render();
		$this->response->appendContent( $response );
	}
	
	
	public function routesList(){
		$this->composer->setData('PageTitle', 'WB PANEL - ROUTES');
		$this->composer->extend('root/body', 'view/wb-panel/routes/routes_list.php');
		$router = new WB_Router();
		$router->loadRoutesFromDir( $GLOBALS['WB_ROUTER_ROUTE_PATH'] );
		$routecollections = $router->getRouteCollections();
		$routes = $router->getRoutes();
		$this->composer->setData('routecollections', $routecollections);
		$this->composer->setData('routes', $routes);
		
		$response = $this->composer->render();
		$this->response->appendContent( $response );
	}

	public function routesSave(){

		$kernel = $GLOBALS['wb_kernel'];
		$kernel->getRouter()->saveRouteCollection($_SERVER['DOCUMENT_ROOT'].'/routes/test.php');
		$this->response->redirect('/wb-panel/routes/view-routes');
	}
	
	public function logs(){
		$this->composer->extend('root/body', 'view/wb-panel/logs/log_view.php' );
		$this->composer->setData('logger', $GLOBALS["WB_LOGGER"]);
		$response = $this->composer->render();
		$this->response->appendContent( $response );
	}
	
	
	/* ================================================================
	
	============================= Setup =============================== 
	
	==================================================================*/
	public function install(){
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
		//require_once($_SERVER['DOCUMENT_ROOT'].'/models/prodottiModel.php');
		//$modelProdotti = new prodottiModel( $database );
		$this->composer->extend('root/body', 'view/wb-panel/setup/install.php');

		$response = $this->composer->render();
		$this->response->appendContent( $response );

	}
	
	
	public function install_create_db(){
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
		//require_once($_SERVER['DOCUMENT_ROOT'].'/models/prodottiModel.php');
		//$modelProdotti = new prodottiModel( $database );
		$database->create_database_structure();
		//sleep(3);
		echo 'success';

		//$this->create_db_status = $_GET['createdb'];


		//$this->status_db = true;
		#$this->response = $this->composer->render();

		#$this->response->appendContent( $response );

	}
	
	/*
	* Funzione per riempire il database con dati
	*/
	public function install_create_data(){
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
		require_once($_SERVER['DOCUMENT_ROOT'].'/models/prodottiModel.php');
		$modelProdotti = new prodottiModel( $database );

		$query = "INSERT INTO tbl_catalogo_prodotti
		( id_tbl_catalogo_prodotti, nome, descrizione, parent)
		VALUES (1,'Root', 'Radice', 0)";
		$database->query( $query );

		$query = "INSERT INTO tbl_listino_prezzi
		( id_tbl_listino_prezzi, nome, descrizione)
		VALUES (1,'Listino di Default', 'Listino prezzi di Default')";
		$database->query( $query );

		//$database->create_database_structure();
		//sleep(2);
		echo 'success';
		#$this->response = $this->composer->render();

		#$this->response->appendContent( $response );

	}
	
	
	public function documenti(){
		$this->composer->extend('body', 'view/documenti.php', '_body');

		$response = $this->composer->render();

		$this->response->appendContent( $response );
		
	}

	public function uninstall(){
		//$this->composer->extend('body', 'view/documenti.php', '_body');
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
		//require_once($_SERVER['DOCUMENT_ROOT'].'/models/prodottiModel.php');
		//$modelProdotti = new prodottiModel( $database );
		//require_once($_SERVER['DOCUMENT_ROOT'].'/view/wb-panel/setup/uninstall.php');
		$this->composer->extend('root/body', 'view/wb-panel/setup/uninstall.php');

		$database->delete_database_structure();
	
		$response = $this->composer->render();
		$this->response->appendContent( $response );
		//$this->response->redirect( '/setup' );

	}

	public function uninstall_status(){
		//$this->composer->extend('body', 'view/documenti.php', '_body');
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
		require_once($_SERVER['DOCUMENT_ROOT'].'/models/prodottiModel.php');
		$modelProdotti = new prodottiModel( $database );
		require_once($_SERVER['DOCUMENT_ROOT'].'/view/setup/uninstall_status.php');

		//$database->delete_database_structure();
		//$response = $this->composer->render();

		//$this->response->redirect( '/setup' );

	}

	public function uninstall_delete_db(){
		//$this->composer->extend('body', 'view/documenti.php', '_body');
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
		require_once($_SERVER['DOCUMENT_ROOT'].'/models/prodottiModel.php');
		$modelProdotti = new prodottiModel( $database );

		$database->delete_database_structure();
		//$response = $this->composer->render();

		$this->response->redirect( '/wb-panel' );

	}


	
	/* ================================================================
	
	============================= Logs =============================== 
	
	==================================================================*/
	
	
	public function version_php(){
		//$this->composer = new WB_Composer( 'view/wb-panel/version_php.php' );
		//$response = $this->composer->render();
		echo phpinfo();
		//$this->response->appendContent( $response );
	}
	
	
	public function prodotti(){
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
		require_once($_SERVER['DOCUMENT_ROOT'].'/models/prodottiModel.php');
		$modelProdotti = new prodottiModel( $database );
		$this->composer->extend('body', 'view/catalogo/catalogo_prodotti.php', '_body');
		$iterator_prodotti = $modelProdotti->getProdotti();

		$this->composer->setData( 'iterator_prodotti', $iterator_prodotti);
		$response = $this->composer->render();
		$this->response->appendContent( $response );
	}
	
	public function dettagli_prodotto(){
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
		require_once($_SERVER['DOCUMENT_ROOT'].'/models/prodottiModel.php');
		$modelProdotti = new prodottiModel( $database );
		
		$id_articolo=$_GET['id_articolo'];
		
		$this->composer->extend('body', 'view/catalogo/dettagli_prodotto.php', '_body');
		
		$iterator_articolo = $modelProdotti->getProdotto( $id_articolo );
		$this->composer->setData( 'iterator_articolo', $iterator_articolo);
		
		$iterator_fornitori = $modelProdotti->getFornitoriArticolo( $id_articolo );
		$this->composer->setData( 'iterator_fornitori', $iterator_fornitori);
		
		$response = $this->composer->render();
		$this->response->appendContent( $response );
	}	
	
	
		/* ================================================================
	
	============================= Modal generation =============================== 
	
	==================================================================*/
	
	
	public function generate_modal($test){
		
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database_structure.php');

		require_once($_SERVER['DOCUMENT_ROOT'].'/view/wb-panel/modalForms/generate_modal.php');
		
		$this->composer->extend('root/body', 'view/wb-panel/modalForms/generate_modal_index.php');
		$this->composer->setData('database_structure', $database_structure);
		
		$response = $this->composer->render();
		$this->response->appendContent( $response );
	}

	public function generate_modal_ex(){
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database_structure.php');

		require_once($_SERVER['DOCUMENT_ROOT'].'/view/wb-panel/modalForms/generate_modal.php');
		
		$response = $this->composer->render();
		$this->response->appendContent( $response );
	}
	
	public function generate_modal_customize(){
		require_once($_SERVER['DOCUMENT_ROOT'].'/view/wb-panel/modalForms/generate_modal_customize.php');
	}
	
	public function generate_modal_file(){
		
		require_once($_SERVER['DOCUMENT_ROOT'].'/view/wb-panel/modalForms/generate_modal_file.php');
	}	

	public function generate_modal_preview(){
		require_once($_SERVER['DOCUMENT_ROOT'].'/view/wb-panel/modalForms/generate_modal_preview.php');
	}		
	
	public function setup(){
		//$this->composer->extend('body', 'view/documenti.php', '_body');
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
		require_once($_SERVER['DOCUMENT_ROOT'].'/models/prodottiModel.php');
		$modelProdotti = new prodottiModel( $database );

		$database->create_database_structure();
		$response = $this->composer->render();

		$this->response->appendContent( $response );
		
	}
	
	
	public function import(){
		$this->composer->extend('body', 'view/catalogo/catalogo_prodotti.php', '_body');
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
		require_once($_SERVER['DOCUMENT_ROOT'].'/models/prodottiModel.php');
		$modelProdotti = new prodottiModel( $database );
		
		$modelProdotti->import_listino_fornitori($_SERVER['DOCUMENT_ROOT'].'/data/listini_fornitori/listino.xml');
		
		$response = $this->composer->render();
		$this->response->appendContent( $response );
	}
	
	
	
		/* ================================================================
	
	============================= Database =============================== 
	
	==================================================================*/
	public function tableList(){
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
		//$this->database = $database;
		$database->connect();
		$this->composer->setData('database', $database);
		$this->composer->extend('root/body', 'view/wb-panel/database/table_list.php');
		$response = $this->composer->render();
		$this->response->appendContent( $response );
		
	}
	
	
	
}//end class


?>

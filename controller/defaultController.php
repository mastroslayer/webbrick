<?php
class defaultController extends WB_Controller{

	public $model;
	public $composer;


	public function __construct(){
		parent::__construct();
		$this->composer = new WB_Composer( 'template/shell_head.php', 'template/shell_body.php' );
        $this->composer->extend( 'header', 'template/app_bar.php', '_body');

	}


	public function index(){
		$this->composer->extend('body', 'view/elements/panel.php', '_body');

		$response = $this->composer->render();

		$this->response->appendContent( $response );

	}

	
	public function documentiAction(){
		$this->composer->extend('body', 'view/documenti.php', '_body');

		$response = $this->composer->render();

		$this->response->appendContent( $response );
		
	}
	
	public function modal_confirm(){
		$this->composer = new WB_Composer( 'template/modal_confirm.php' );
		$response = $this->composer->render();
		$this->response->appendContent( $response );
	}

	public function prodotti(){
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
		require_once($_SERVER['DOCUMENT_ROOT'].'/models/prodottiModel.php');
		$modelProdotti = new prodottiModel( $database );
		$this->composer->extend('body', 'view/catalogo/catalogo_prodotti.php', '_body');
		$iterator_prodotti = $modelProdotti->getProdotti();

		$this->composer->setData( 'iterator_prodotti', $iterator_prodotti);
		$response = $this->composer->render();
		$this->response->appendContent( $response );
	}
	
	public function dettagli_prodotto(){
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
		require_once($_SERVER['DOCUMENT_ROOT'].'/models/prodottiModel.php');
		$modelProdotti = new prodottiModel( $database );
		
		$id_articolo=$_GET['id_articolo'];
		
		$this->composer->extend('body', 'view/catalogo/dettagli_prodotto.php', '_body');
		
		$iterator_articolo = $modelProdotti->getProdotto( $id_articolo );
		$this->composer->setData( 'iterator_articolo', $iterator_articolo);
		
		$iterator_fornitori = $modelProdotti->getFornitoriArticolo( $id_articolo );
		$this->composer->setData( 'iterator_fornitori', $iterator_fornitori);
		
		$response = $this->composer->render();
		$this->response->appendContent( $response );
	}	
	
	
	

	
	
	public function import(){
		$this->composer->extend('body', 'view/catalogo/catalogo_prodotti.php', '_body');
		require_once($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
		require_once($_SERVER['DOCUMENT_ROOT'].'/models/prodottiModel.php');
		$modelProdotti = new prodottiModel( $database );
		
		$modelProdotti->import_listino_fornitori($_SERVER['DOCUMENT_ROOT'].'/data/listini_fornitori/listino.xml');
		
		$response = $this->composer->render();
		$this->response->appendContent( $response );
	}
	
	
}//end class


?>

<?php
/*
 * Authore: Alessandro Carrer
 *
 * To do:
 * Parsing e controllo su function redirect
 *
 * Version: 0.1
*/


class exceptionController extends WB_Controller{

	public $template = 'template/exception.php';


	public function __construct(){
		parent::__construct();
	}


	public function Action(){


	}

	public function genericExceptionAction( $code=0, $message='Undefined Generic Error', $file_name='0', $line='0'){

		//include $_SERVER['DOCUMENT_ROOT'].'template/exception.php';
		/*$composer = new WB_Composer( null, 'template/exception.php');

		$composer->setData( 'code', $code);
		$composer->setData( 'message', $message);
		$composer->setData( 'file', $file);
		$composer->setData( 'line', $line);*/
		$file = $_SERVER['DOCUMENT_ROOT'].'/template/exception.php';
		$this->startRender();
		if( file_exists( $file ) ){
			include $file;
		}else{
			throw new Exception("Controller cannot load file");
		}

		$response = $this->endRender( );

		$this->response->appendContent( $response );

	}



	public function error404Action( ){
		$template = new WB_Template('template/error.php');

		//$template->setData( 'bodyTemplate', 'dataset', $dataset);

		$response = $template->render();

		$this->response->appendContent( $response );
	}


}//end class
?>

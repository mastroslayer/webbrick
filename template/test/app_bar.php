<?php $this->startBlock('AppBar', 'css'); ?>

<?php $this->endBlock(); ?>


<?php $this->startBlock('AppBar');

?>

<style>

	
	#ApplicationMenuButton:hover .ApplicationMenu{
		display: block;
		display: block;
		list-style-type: none;
		background: white;
		color: black;
		position: absolute;
		margin: 0px;
		padding: 0px;
		right: 16px;
		box-shadow: 0px 0px 1px 2px lightgrey;
		padding: 4px 0px 4px 0px;
	}

	.ApplicationMenu{
		display: none;
	}
	
	.ApplicationMenu.show{
		display: block;
		list-style-type: none;
		background: white;
		color: black;
		position: absolute;
		margin: 0px;
		padding: 0px;
		right: 16px;
		box-shadow: 0px 0px 1px 2px lightgrey;
		padding: 8px;
	}
	
	.ApplicationMenu > li{
		padding: 8px 16px 8px 16px;
		font-size: 18px;
		border-bottom: 1px solid lightgray;
		cursor: pointer;
	}
	
	.ApplicationMenu > li:hover{
		background-color:  #dedede69;
	}
	
</style>

<div id="WBAppBar" class="wb-app-bar">
	<div class="body">

		<ul class="wb-toolbar left">
			<li id="WBAppBarNav" onclick="wb_toogle_sidenav('wbAppSideNav', 'left');"><img src="/imgs/icons/outline-menu-white-24px.svg"></li>
			<li><?php //echo $this->getData('WBAppBarTitle'); ?></li>
			<!-- <li>
				<select>
					<option>Selezione 1</option>
					<option>Selezione 2</option>
					<option>Selezione 3</option>
				</select>
			</li> -->
		</ul>

		<ul class="wb-toolbar right">
			<li id="ApplicationMenuButton" onclick="wb_AppBarNavOpen('ApplicationMenu');">
				<img src="/imgs/icons/outline-apps-white-24px.svg">
				<ul id="ApplicationMenu" class="ApplicationMenu" style="width:170px;">
					<li><a href="/anagrafica/">Anagrafica</a></li>
					<li><a href="/prodotti/" style="display:inline-block;"><img src="/imgs/icons/products.png" style="width:18px;vertical-align:middle;padding-right:8px;">Catalogo</a></li>
					<li><a href="/documenti">Documenti</a></li>
				</ul>
			</li>
		</ul>

	</div>
</div>
<div class="wb-app-bar-spacer"></div>
<?php $this->endBlock(); ?>

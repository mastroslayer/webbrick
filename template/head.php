<?php $this->startBlock('head'); ?>

<title><?php echo $this->getData('PageTitle', 'Page Title'); ?></title>

<!-- Include viewport Metatag  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<link rel="manifest" href="/js/manifest.json">

<!-- Include Android Metatag -->
<meta name="mobile-web-app-capable" content="yes">
<link rel="icon" sizes="192x192" href="/iphone_webapp_icon.png">


<!-- Include Safari & Iphone Metatag -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="apple-mobile-web-app-title" content="">

<!-- <link rel=icon href=favico.png sizes="16x16" type="image/png"> -->


<!-- Include Font -->
<link rel="stylesheet" type="text/css" href="/fonts/Roboto/Roboto.css">
<link rel="stylesheet" type="text/css" href="/fonts/Quicksand/Quicksand.css">
<link rel="stylesheet" type="text/css" href="/fonts/Material-design-icons/material-icons.css">

<link rel="stylesheet" type="text/css" href="/css/webbrick/wb_layout.css">
<link rel="stylesheet" type="text/css" href="/css/webbrick/wb_components.css">


<link rel="stylesheet" type="text/css" href="/css/webbrick/wb_shell_layout.css">
<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="stylesheet" type="text/css" href="/css/wb-panel.css">
<script type="text/javascript" src="/js/wb_components.js"></script>

<style>
<?php //$this->printGlobal('css'); ?>
</style>

<script>
<?php //$this->printBlock('_global', 'javascript'); ?>
</script>

<script>
	function wb_AppBarNavOpen(navID){
		var nav = document.getElementById(navID);
		if(nav){
			nav.classList.add('show');
		}
	}

	function wb_AppBarNavClose(navID){
		var nav = document.getElementById(navID);
		if(nav){
			nav.classList.remove('show');
		}
	}

	function wb_toogle_sidenav(navID, direction){
		var nav = document.getElementById(navID);
		if(nav){
			if( nav.classList.contains('show') ){
				nav.classList.remove('show');
			}else{
				nav.classList.add('show');
			}
		}
	}
	/*
	function wb_dialogWindowOpen( urlAction ){

			$.ajax({url: urlAction,
				success: function(result){
					$("#wb-dialog-window-body").html(result);
					$("#wb-dialog-window").addClass('display');
					$("body").addClass('disable-scroll');
				},
				error:function(result){
					$("#wb-dialog-window-body").html("<h3>Error</h3>");
					$("#wb-dialog-window").addClass('display');
					$("body").addClass('disable-scroll');
				},
			});

	}

	function wb_dialogWindowClose( ){
		$("#wb-dialog-window").removeClass('display');
		$("body").removeClass('disable-scroll');
	}
	*/
</script>

<?php $this->endBlock(); ?>
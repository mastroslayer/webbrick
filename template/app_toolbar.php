<?php $this->startBlock('AppToolBar'); ?>
<div id="WBAppToolbar" class="wb-app-toolbar">
	<div class="body">
		
		<ul class="wb-toolbar left">
			<li>
				<div style="width:1px; height:64px;"></div>
			</li>
			<li><?php echo $this->getData('WBAppBarTitle'); ?></li>
			<?php $this->printBlock('AppToolbarLeft'); ?> 
		</ul>
		
		
		<ul class="wb-toolbar right">
			<?php $this->printBlock('appToolbarRight'); ?>
			<?php $this->printBlock('AppToolbarRight'); ?>
		</ul>
		
		<?php $this->printBlock('AppToolbarLevel2'); ?>

	</div>
</div>
<div class="wb-app-bar-spacer"></div>
<?php $this->endBlock(); ?>
<?php $this->output('AppBar', 'Il mio header'); ?>
<?php $this->output('toolbar', 'Bho'); ?>




<script>
	function wb_AppBarNavOpen(navID){
		var nav = document.getElementById(navID);
		if(nav){
			nav.classList.add('show');
		}
	}

	function wb_AppBarNavClose(navID){
		var nav = document.getElementById(navID);
		if(nav){
			nav.classList.remove('show');
		}
	}

	function wb_toogle_sidenav(navID, direction){
		var nav = document.getElementById(navID);
		if(nav){
			if( nav.classList.contains('show') ){
				nav.classList.remove('show');
			}else{
				nav.classList.add('show');
			}
		}
	}
	/*
	function wb_dialogWindowOpen( urlAction ){

			$.ajax({url: urlAction,
				success: function(result){
					$("#wb-dialog-window-body").html(result);
					$("#wb-dialog-window").addClass('display');
					$("body").addClass('disable-scroll');
				},
				error:function(result){
					$("#wb-dialog-window-body").html("<h3>Error</h3>");
					$("#wb-dialog-window").addClass('display');
					$("body").addClass('disable-scroll');
				},
			});

	}

	function wb_dialogWindowClose( ){
		$("#wb-dialog-window").removeClass('display');
		$("body").removeClass('disable-scroll');
	}
	*/
</script>


<div id="wbAppSideNav" class="wb-app-sidenav left ">
		<div class="header">
			<ul class="wb-toolbar right">
				<li id="WBAppBarNav" onclick="wb_AppBarNavClose('wbAppSideNav');"><img src="/img/icons/scalable/ic_clear_black_48px.svg"></li>
			</ul>
		</div>
		<div class="body">
			<section>
				<ul class="wb-sidenav-list" style="">
					<li style=""> <img src="/img/icons/scalable/ic_people_black_48px.svg"><a href="/admin/dashboard.html">ANAGRAFICA</a></li>
					<li style=""> <img src="/img/icons/scalable/ic_people_black_48px.svg"><a href="/admin/prodotti">CATALOGO</a></li>
					<li><img src="/img/icons/categorie.svg"><a href="/admin/categorie.html">DOCUMENTI</a></li>
					<li class="selected"><img src="/img/icons/product.svg"><a href="/admin/prodotti.html">BACKOFFICE</a></li>
					<li><img src="/img/icons/scalable/ic_room_service_black_48px.svg"><a href="/admin/ricette.html">IMPOSTAZIONI</a></li>
					<li><img src="/img/icons/scalable/ic_exit_to_app_black_48px.svg"><a href="/admin/logout.html">LOGOUT</a></li>
				</ul>
			</section>
		</div>
	</div>


	<div id="" class="wb-app-sidenav right pinned" style="display:none;">
		<div class="header">
			<ul class="wb-toolbar left">
				<li id="WBAppBarNav" onclick="wb_AppBarNavOpen('wbAppSideNav');"><img src="/img/icons/scalable/ic_menu_white_48px.svg"></li>
			</ul>
		</div>
		<div class="body">
			<ul class="wb-sidenav-list" style="">
				<li style="" onclick="wb_AppBarNavClose( 'wbAppSideNav' );"><span>CLOSE</span></li>
				<li style=""><span>DASHBOARD</span></li>

				<li style=""><span>UTENTI</span></li>
				<li><span>CLIENTI</span></li>
				<li class="selected"><a href="/admin/prodotti.html">PRODOTTI</a></li>
				<li><a href="/admin/categorie.html">CATEGORIE</a></li>
			</ul>
		</div>
	</div>

		<div id="wbAppBody" class="">
		        <?php $this->output('body', '<h3>My Default body</h3>'); ?>
		</div>

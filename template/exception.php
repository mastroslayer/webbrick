<html>
<head>
	<title>WebBrick Error</title>

	<!-- Include viewport Metatag  -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />


	<!-- Include Android Metatag -->
	<meta name="mobile-web-app-capable" content="yes">
	<!-- <link rel="icon" sizes="192x192" href="iphone_webapp_icon.png"> -->


	<!-- Include Safari & Iphone Metatag -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	<meta name="apple-mobile-web-app-title" content="WebBrick Error">

	<link rel="stylesheet" type="text/css" href="/fonts/Quicksand/Quicksand.css">
<style>

	body{
		background-color:blue;
		font-family: Quicksand;
	}

	.text{
		color:white;
		text-align:center;
		margin:16px;
		font-family: Quicksand;
		max-width: 100%;

	}
.wb-dialog.form .wb-dialog-window{
	background-color: #00cbfb;
}
	.error-code{
		font-size: 55px;
	}

	.error-message{
		font-size: 30px;
		//max-width:300px;
		font-weight: 400;
	}


	.error-file{
		font-weight: 400;
		font-size: 20px;
		word-break:break-all;
	}

	.error-line{
		font-weight: 400;
		font-size: 20px;

	}

	.back-button{
		border: 2px solid white;
		border-radius: 32px;
		padding: 16px 32px 16px 32px;
		text-align: center;
		color: white;
		font-weight: 800;
		cursor: pointer;
		margin-left: auto;
		margin-right: auto;
		max-width: 200px;
		margin-top: 64px;
	}
</style>
</head>
<body>
		<h1 class="text error-code"><?php echo $code; ?></h1>
		<h1 class="text error-message"><?php echo $message; ?></h1>
		<h1 class="text error-file"><?php echo 'File: '.$file; ?></h1>
		<h1 class="text error-line"><?php echo 'Riga: '.$line; ?></h1>
		<div class="back-button" onclick="window.history.back();">INDIETRO</div>
</body>
</html>

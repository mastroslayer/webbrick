<?php
/*
 * Authore: Alessandro Carrer
 *
 * To do:
 * Parsing e controllo su function redirect
 *
 * Version: 0.1
*/
class WB_Controller{


	private $debug_log;
	public $debug;
	public $response;
	public $request;

	
	/* Costruttore */
	public function __construct( ) {
		$this->logger = $GLOBALS["WB_LOGGER"];

		$this->debug = true;
		$this->response = new WB_HttpResponse();
		//$this->request = new WB_HttpRequest();
	}



	public function includePhp( $file ){
		if( file_exists( $_SERVER['DOCUMENT_ROOT'].$file ) ){
			include $_SERVER['DOCUMENT_ROOT'].$file;
		}else{
			throw new Exception('Impossibile includere file');
		}
	}



	public function startRender(){
		ob_start();
	}

	public function endRender(){
		$buffer= ob_get_contents();
		ob_end_clean();
		return $buffer;
	}

	public function renderPage( $file ){
		ob_start();
		if( file_exists( $file ) ){
			include $file;
		}else{
			throw new Exception("Controller cannot load file");
		}
		$buffer= ob_get_contents();
		ob_end_clean();
		return $buffer;
	}


	public function redirect( $url ){
		header('location:'.$url);
		//$this->response->setLocation($url);
		die();
	}



	public function getResponse(){
		return $this->response;
	}
	
	
	
/* --------------------------------------------------------
 * Leggi tutti i parametri post
 *
 * ------------------------------------------------------*/
/*
public function readPOST($field, $prefix){
print_r($field);
	$size=count($field);
	$parm = null;
	$fieldIndex = array_keys($field);
	if($this->debug){
		echo 'Numero parametri:'.$size.'<br>';
	}
	for($x=0; $x < $size; $x++) {
		if($this->debug){ echo 'Lettura POST:'.$prefix.$field[$fieldIndex[$x]]->name.'<br>';}
		
		if(isset($_POST[$prefix.$field[$fieldIndex[$x]]->name])){
			if($this->debug){ echo 'Trovato POST:'.$prefix.$field[$fieldIndex[$x]]->name.'='.$_POST[$prefix.$field[$fieldIndex[$x]]->name].'<br>';}
			$parm[$field[$fieldIndex[$x]]->name] = $_POST[$prefix.$field[$fieldIndex[$x]]->name];
		}
		
		if(isset($_POST[$prefix.$field[$fieldIndex[$x]]->name_encripted])){
			if($this->debug){ echo 'Trovato POST encripted:'.$prefix.$field[$fieldIndex[$x]]->name_encripted.'='.$_POST[$prefix.$field[$fieldIndex[$x]]->name].'<br>';}
			$parm[$field[$fieldIndex[$x]]->name] = $_POST[$prefix.$field[$fieldIndex[$x]]->name_encripted];
		}	
	}

	if($this->debug){
		echo 'Stampo parametri POST <br>';
		foreach($parm as $key => $value ) {
			echo "Name: ".$key."<br>";
			echo "Value: ".$value."<br>";
		}
	}
	return $parm;

}
*/

public function readPOST($field, $prefix){
	$size=count($field);
	$parm = null;
	$this->logger->log('wb_controller', 'Reading post parameter', 3);
	$this->logger->log('wb_controller', 'Try to read '.$size.' parameter', 3);

	/*if($this->debug){
		echo 'Numero parametri:'.$size.'<br>';
	}*/
	for($x=0; $x < $size; $x++) {
		$this->logger->log('wb_controller', 'Reading post '.$prefix.$field[$x], 3);

		if(isset($_POST[$prefix.$field[$x]])){
			//if($this->debug){ echo 'Trovato POST:'.$prefix.$field[$x].'='.$_POST[$prefix.$field[$x]].'<br>';}
			$this->logger->log('wb_controller', 'Found post parameter '.$prefix.$field[$x].'='.$_POST[$prefix.$field[$x]], 3);
			
			$parm[$field[$x]] = $_POST[$prefix.$field[$x]];
		}
			
	}
	
	return $parm;

}	
	
	
public function setRequest( $request ){
	$this->request=$request;
}



}//end class
?>

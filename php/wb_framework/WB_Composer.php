<?php 
/**
*
* Component contengono un solo blocco
* 
*
* $composer = new Composer( template/test/skel.php);
* 
* $composer->extend(root/header);
* $composer->extend(root/appBody);
* $composer->extend(root/appBody/header);
* $composer->extend(root/appBody/body);
*
*
* $composer->loadComponent();
*
* Special blocks:
* global_css
* global_javascript
* 
* $this->startBlock( 'body' );
* $this->endBlock();
*/

class WB_Composer{
	
		/*
		* $structure['name']['file']
		*                   ['childs']
		*/
		private $views; 
		private $blocks;
		
		private $head_file;	
	
		private $runtime_block_name;
	
		private $data;
		private $logger = null;
	
	
	public function __construct( $skel_file ) {
		$this->logger = $GLOBALS["wb_logger"];

		$this->blocks = array();
		$this->views = array();
		$this->views['root']['file'] = $skel_file;
		$this->head = '';
		$this->runtime_block_name = null;
		$this->data = array();
		//$this->structure['_body'][]
	}
	
	
	public function extend( $full_name, $file ){
		//echo 'FULLNAME:'.$full_name;
		$full_name_tok = explode("/", $full_name);
		$depth = count($full_name_tok);
		if( $depth > 1){
			$name = $full_name_tok[$depth-1];
			//echo 'NAME:'.$name;
			$parent = substr($full_name, 0, -strlen($name)-1);
			//echo 'PARENT:'.$parent;
			//print_r( $this->structure);
			$this->addChild( $name, $file, $parent);
		}else{
			$this->addChild( $full_name, $file);
		}
		//print_r( $this->structure);
	}
	
	
	/*
	* Function: addChild
    * @param $name: Child name
    * @param $file: File path
    * @param $parent: parent to extend
    * Default:body
	*/
	public function addChild( $name, $file, $parent='root' ){
		$this->logger->log('wb_composer', 'Add child template '.$name.' to:'.$parent, 3);

		//echo 'Add Child template:'.$name.' to:'.$parent.'<br>';
		$full_name = $parent.'/'.$name;
		if( !array_key_exists($parent, $this->views) ){
			throw new Exception("Unable to add child ".$name." parent ".$parent." doesn't exists", 1);
		}
		if( !file_exists($file) ){
			throw new Exception("Unable to add child file $file doesn't exists", 1);
		}
		$this->views[ $parent ]['childs'][]= $full_name;
		$this->views[ $full_name ] = array( 'file'=>$file,  'childs'=>array() );
	}
	
	
	/*
	* Genera la lista dei file da renderizzare nel corretto ordine
	* viene richiamata prima di iniziare il rendering
	*/
	private function generate_render_list( $parent='root', &$render_list){
		/*if($render_list == null){
			$render_list = array();
		}*/
		$render_list[] = $parent;
		if( array_key_exists( $parent, $this->views) ){
			$childs = $this->views[ $parent ][ 'childs' ];
            if( ($childs != null) && (count($childs) > 0) ){
                foreach($childs as $child){
                    $this->generate_render_list( $child, $render_list );
                    //$render_list = array_merge($render_list, $tmp);
                }
            }
		}


		//return $render_list;
	}
	
	
	public function render(){
		
		$render_list = array();
		$output = '';
		$this->generate_render_list('root', $render_list);
		//print_r($render_list);
		$count = count($render_list)-1;
		while( $count > -1){
			
			$file = $this->views[ $render_list[$count] ]['file'];
			if( !file_exists($file) ){
				throw new Exception("Rendering composer: file".$file." not exists", 1);
			}
			$this->logger->log('wb_composer', 'Rendering '.$file, 3);

			//echo 'including '.$file.'<br>';
			if($count == 0){
				ob_start();
				
				include $file;
				$output = ob_get_contents();
				ob_end_clean();
			}else{
				include $file;
			}
			$count--;
		}
		
		$output_head = '';
		if( $this->head_file != null){
			ob_start();
				
			include $this->head_file;
			$output_head = ob_get_contents();
			ob_end_clean();
		}
		
		
		if( $this->head_file != null){
			
			
		}
		/*
		ob_start();
		$buffer_body = ob_get_contents();
		ob_end_clean();
		*/
		return $output;
		
		
		
	}
	
	
	public function startBlock( $blockName, $mergeMethod='append' ){
		$this->runtime_block_name = $blockName;
		
		if( $this->runtime_block_name == null ){
			throw new Exception("composer, start, Block layer not started", 1);
		}	
		
		if(ob_get_level() > 1){
            throw new Exception( 'Bufferig already started');
            //echo 'Buffering already started';
        }
        
		if( !array_key_exists($blockName, $this->blocks) ){
			$this->blocks[$blockName] = new WB_Block(); 
		}
		
		ob_start();
	}
	
	
	public function printBlock( $blockName ){
		if( array_key_exists($blockName, $this->blocks) ){
			$this->blocks[$blockName]->printContent(); 
		}
	}
	
	
	public function endBlock( ){
		if( $this->runtime_block_name == null ){
            throw new Exception( 'Errore block never started');
		}
		
		$buffer = ob_get_contents();
		ob_end_clean();
		$this->blocks[$this->runtime_block_name]->append($buffer);
		
		$this->runtime_block_name = null;
	}
	
	
	public function getData( $name, $alt=null ){
		if( array_key_exists($name, $this->data) ){
			return $this->data[$name]; 
		}else{
			return $alt;
		}
	}
	
	public function setData( $name, $data ){
		$this->data[$name] = $data; 
	}
	
	
}



class WB_Block{
	
	
	private $data;
	
	
	public function __construct(){
		$this->data = array();
		$this->data['html'] = '';
	}
	
	
	public function append( $data, $type='html'){
		$this->data['html'] .= $data;
		
	}
	
	
	public function getContent(){
		return $this->data['html'];
	}
	
	
	public function printContent(){
		echo $this->data['html'];
	}
}

?>
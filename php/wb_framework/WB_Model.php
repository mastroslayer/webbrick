<?php


class WB_Model{



	public function __construct() {


	}



	/* ----------------------------------------------------------------------------------------
	 * Utility
	 *
	*/
	public function search_file( $name, $extension, $default){

        foreach( $extension as $ext ){
            $n = $name.'.'.$ext;
            //echo 'cerco: '.$n.'<br>';
            if( file_exists($_SERVER['DOCUMENT_ROOT'].$n) ){
                //echo 'found<br>';
                return $n;
            }
        }

        return $default;

    }




	public function deleteFiles( $files, $path=null ){
		if( is_array($files) ){
			foreach( $files as $file ){
				$this->deleteFile($file, $path);
			}
		}else{
			$this->deleteFile($file, $path);
		}
	}


	public function deleteFile( $file, $path=null){
		if($path != null ){
			$f_path = $path.$file;
		}else{
			$f_path = $file;
		}

		if( file_exists($f_path) ){
			unlink($f_path);
		}
	}


	public function deleteDir( $dir ) {
		$files = array_diff(scandir($dir), array('.','..'));
		foreach ($files as $file) {
			if(is_dir("$dir/$file")){
				$this->deleteDir("$dir/$file");
			}else{
				unlink("$dir/$file");
			}
		}
		if( is_dir($dir) ){
			rmdir($dir);
			return true;
		}else{
			return false;
		}
	}




	public function createDir($dir, $create_tree) {

	}


	/* Get all files inside a directory
	* @param $dirPath
	*/
	public function getFiles( $dirPath ){
		$img_list = null;
		if( is_dir($dirPath) ){
			$img_list = scandir($dirPath);
			array_splice($img_list, 0, 2);
		}
		return $img_list;
	}


	/*
	 * Cerca tutti i file con una determinata estensione
	 * @param type - array - estensione file da cercare senza il .
	 * @param path - string - percorso cartella dove effettuare la ricerca
	 * @return - restituisce un array con i nomi dei file senza il percorso
	*/
	public function getFilesByType( $path, $type=null, $append=null){
			$galleryDir = $path;
			$result = array();
			$img_list = null;

			if(is_dir($galleryDir)){

				$img_list = scandir($galleryDir);
				//print_r($img_list);
				array_splice($img_list, 0, 2);
			}
			//echo 'ok';
			if(count($img_list) > 0){
				//echo 'ok2';
				foreach ($img_list as $img){
					foreach ($type as $t){
						//echo 'comparo:'.$img.' con '.$t.'<br>';
						if(pathinfo($img,  PATHINFO_EXTENSION) == $t){
							if($append != null){ $result[] = $append.$img; }
							else{$result[] = $img;}

						}
					}

				}
			}
			return $result;
	}




	/*
	 * Cerca tutti i file con una determinata estensione
	 * @param type - array - estensione file da cercare senza il .
	 * @param path - string - percorso cartella dove effettuare la ricerca
	 * @return - restituisce un array con i nomi dei file senza il percorso
	*/
	public function getFilesByName( $path, $type=null, $append=null){
			$galleryDir = $path;
			$result = array();
			$img_list = null;

			if(is_dir($galleryDir)){

				$img_list = scandir($galleryDir);
				//print_r($img_list);
				array_splice($img_list, 0, 2);
			}
			//echo 'ok';
			if(count($img_list) > 0){
				//echo 'ok2';
				foreach ($img_list as $img){
					//echo 'elaboro'.$img;
					foreach ($type as $t){
						//echo 'comparo:'.$img.' con '.$t.'<br>';
						$name = substr($img, 0, strrpos($img, "."));
						//if(pathinfo($img, PATHINFO_FILENAME) == $t){
						//echo 'comparo:'.$name.' con '.$t.'<br>';

						if( $name == $t){
							//echo 'aggiungo<br>';
							if($append != null){ $result[] = $append.$img; }
							else{$result[] = $img;}

						}
					}

				}
			}
			//print_r($result);
			return $result;
	}




}
?>

<?php
/*
 * Authore: Alessandro Carrer
 *
 * To do:
 *
 * Version: 0.1
 *
 *
 * Usage:
 *
 * requirement structure
 * array(
 *       'validate'=>'regexExpression' - default field name
 *       'exp_start'=>'start regexExp' -default '/'
 *       'exp_end'=>'end regexExp' - default '/'
 *	   'exp_end'=>'end regexExp' - default '/
 *   )
*/
class WB_Route{

	// controller class name
	public $controller;

	// path to controller class file
	public $controllerPath;

	// controller class method name
	public $action;

	// url to match the route
	public $url;

	// url field
	public $fields;
	//fields
	/*'name'=>array(
	'validate'=>'regexExpression' - default field name
	'exp_start'=>'start regexExp' -default '/'
	'exp_end'=>'end regexExp' - default '/'
	'value'=>'end regexExp' - default '/'
		'default'=>'defaultValue'
		''
	);
    */

	//type can be GET POST PARM
	private $function_parameter; //lista dei parametri da passare
	private $get_parameter;
	private $post_parameter;




	/* Contructor
	* @param url - the route url
	* @param default - the default values
	* @param requirements - requirement field
	*/
	public function __construct( $url, $default, $requirement=array(), $parameter= array() ) {
		$this->url = $url;
		$this->fields = array();
		$this->function_parameter = array(); //lista dei parametri da passare
		$this->get_parameter = array();
		$this->post_parameter = array();
		$this->loadFields( $requirement );

		$this->loadDefaults( $default );
		//print_r($this->fields);
	}


	/*
	 * Carica la configurazione
	 * Carica il controllore e e l'azione del controllore
	*/
	private function loadDefaults( $config ){
		if(array_key_exists('action', $config)){
			$this->action = $config['action'];
		}else{
			$this->action = 'index';
		}

		if(array_key_exists('controller', $config)){
			$this->controller = $config['controller'];
		}else{
			$this->controller = null;
		}

		if(array_key_exists('controllerPath', $config)){
			$this->controllerPath = $config['controllerPath'];
		}else{
			$this->controllerPath = null;
		}
	}

	/*
	* Carica la lista dei parametri da passare
	*/
	private function loadParameter( $parameter ){
		foreach( $parameter as $param){
			$this->parameter[] = $param;
		}
	}

	/*
	 * Read fields an load all field configuration
	*/
	private function loadFields( $fields ){
		$token = strtok($this->url, "/");
		while ($token !== false){
			//echo 'carico Campo:'.'';
			$this->fields[] = $this->loadField( $token, $fields);
			$token = strtok("/");
		}

	}


	/*
	 * @field Richiede nome token campo
	 * @opt array configurazione
	 */
	private function loadField( $field, $opt ){
		$tmpField = array();
		$tmpField['name'] = $field;

		if(array_key_exists($field, $opt) ){ //Esiste configurazione del campo

			if( array_key_exists('validate', $opt[$field]) ){
				$tmpField['exp'] = $opt[$field]['validate'];
			}else{
				$tmpField['exp'] = $field;
			}

			if( array_key_exists('exp_start', $opt[$field]) ){
				$tmpField['exp_start'] = $opt[$field]['exp_start'];
			}else{
				$tmpField['exp_start'] = '/\b';
			}

			if( array_key_exists('exp_end', $opt[$field]) ){
				$tmpField['exp_end'] = $opt[$field]['exp_end'];
			}else{
				$tmpField['exp_end'] = '\b/';
			}

			if( array_key_exists('type', $opt[$field]) ){
				$tmpField['type'] = $opt[$field]['type'];
			}else{
				$tmpField['type'] = 'static';
			}


		}else{
			$tmpField['exp'] = $field;
			$tmpField['exp_start'] = '/\b';
			$tmpField['exp_end'] = '\b/';
			$tmpField['type'] = 'static';
		}

		return $tmpField;

	}


	/*
	* Check if the route corrispond to the url
	* @param urlString
	* @return True if the url match or False if doesn't
	*/
	public function match( $urlString ){
		$urlTokens = array();
		$urlTokens = $urlString;
		/*
		$token = strtok($urlString, "/");
		while ($token !== false){
			$urlTokens[] = $token;
			//echo $token;
			$token = strtok("/");
		}
		*/
		if( count($urlTokens) == count($this->fields) ){
			$count = count($urlTokens);
			$i = 0;
			$match = true;

			while( ($i<$count) && ($match == true) ){

				//echo 'comparo:'.$this->fields[$i]['exp'].' con '.$urlTokens[$i].'<br>';
				//Test if token match
				//if(preg_match('/'.$this->fields[$i]['exp'].'/', $urlTokens[$i])){
				$exp = $this->fields[$i]['exp'];
				$start = $this->fields[$i]['exp_start'];
				$end = $this->fields[$i]['exp_end'];
				//echo 'testo:'.$start.$exp.$end.'<br>';
				if( $this->fields[$i]['type'] == 'get'){
					$this->get_parameter[ $this->fields[$i]['name'] ] = $urlTokens[$i];
				}elseif ($this->fields[$i]['type'] == 'post') {
					$this->post_parameter[ $this->fields[$i]['name'] ] = $urlTokens[$i];
				}elseif ($this->fields[$i]['type'] == 'function') {
					$this->function_parameter[ $this->fields[$i]['name'] ] = $urlTokens[$i];
				}

				if(preg_match($start.$exp.$end, $urlTokens[$i])){
					$match = true;
					//echo ' MATCH';
				}else{
					//echo ' NO MATCH';
					$match =false;
				}

				$i++;
			}
		}else{
			return false;
		}
		return $match;
	}


	
	/*
	* Set the parameter to pass to the controller
	*/
	public function setParameter( $fieldName, $type){
		
		
		
	}
	
	
	/*
	* Set the field to validate with regexp
	* @param $fieldName the field name to validate
	* @param $expression the regex for validation
	*/
	public function validate( $fieldName, $expression){
		$this->fields[$fieldName] = $expression;
	}
	
	/*
	* @return name of the controller class
	*/
	public function getController(){
		if($this->controller == null){return null;}else{
		return $this->controller;}
	}


	public function setController( $controller ){
		$this->controller = $controller;
	}


	/*
    * @return file path of the controller class file
    */
	public function getControllerPath(){
		return $this->controllerPath;
	}

	public function setControllerPath( $controllerPath ){
		$this->controllerPath = $controllerPath;
	}

	

	

	/*
    * @return method to call from the controller class
    */
	public function getAction(){
		return $this->action;
	}

	public function setAction( $action ){
		$this->action = $action;
	}

	public function getFunctionParameter(){
 		return $this->function_parameter;

	}

	public function setEnviromentParameter(){
		foreach( $this->get_parameter as $key=>$value ){
			$_GET[$key] = $value;
		}

		foreach( $this->post_parameter as $key=>$value ){
			$_POST[$key] = $value;
		}

	}

	public function getFileString(){
		$txt = "new WB_Route(";
		$txt .= "'$this->url',"."\n";
		$txt .= "\tarray(";
		$txt .= "action=>'".$this->action."'),\n";
		
		$empty = true;
		foreach ($this->fields as $field){
			
			if($field['type'] != 'static'){
				if($empty == true){$txt .= "\tarray(";}
				
			$txt .= "\t'".key($field)."'=>array(";
			
			foreach($field as $tmpKey=>$tmpVal){
				
				$txt.= "'$tmpKey'=>'$tmpVal',";
			}
			if($empty == false){$txt .= ")\n,";}
			}
			
		}
		$txt .= "),";
		return $txt;
		
	}
	
	public function printDebug(){
		echo '<br>Route:'.$this->url.'<br>';
		echo 'Controller:'.$this->controller.'<br>';
		echo 'Path:'.$this->controllerPath.'<br>';
		echo 'Action:'.$this->action.'<br>';
		foreach( $this->fields as $field){
			echo 'route field - name:'.$field['name'];
			echo ' exp:'.$field['exp'];
			echo '<br>';
		}
	}
}//end class












?>

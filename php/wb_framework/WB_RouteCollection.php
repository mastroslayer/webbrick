<?php 
/*
 * Authore: Alessandro Carrer
 *
 * To do:
 * Version: 1.0
 *
 * WB_RouteCollection
 * La classe rappresenta una collezione di rotte
 *
 * 
*/

class WB_RouteCollection{

	private $defaultController;		//Nome classe Controller
	private $defaultControllerPath; //Percorso file

	private $baseUrl;
	private $routes;

	private $index;

	public function __construct( $baseUrl='') {
		$index = 0;
		$this->routes = array();
		$this->baseUrl = $baseUrl;
		$this->defaultController = null;
		$this->defaultControllerPath = null;
	}


	public function add( $route ){
		//print_r($route);

		if( $route->getController() == null){
			//echo '<br>il controller era null<br>';
			$route->setController( $this->defaultController );
			$route->setControllerPath( $this->defaultControllerPath );
			$this->routes[] = $route;
			//echo 'Rotta';
			//print_r($route);
		}else{
			$this->routes[] = $route;
		}
		//$route->printDebug();
		/*
		echo 'Rotta';
		print_r($route);
		echo '<br>Rotte:';
		print_r($this->routes);*/
	}

	public function setController( $path, $controller ){
		$this->defaultController = $controller;
		$this->defaultControllerPath = $path;
	}

	public function getRoutes(){
		return $this->routes;

	}

	public function getFileString(){
		$txt = "";
		foreach($this->routes as $route){
			$txt .= '$routeCollectionName->add(';
			$txt .= $route->getFileString()."\n";
			$txt .= ")\n";
			
		}
		return $txt;
	}

	
	/*
	* Carica la classe del controller
	*/
	public function loadController(){
		
		if( file_exists($_SERVER['DOCUMENT_ROOT'].$this->defaultControllerPath) ){
			require_once($_SERVER['DOCUMENT_ROOT'].$this->defaultControllerPath);
			
		}else{
			throw new Exception('Route Collection class file:'.$this->defaultControllerPath.' not found');
		}
	}
	
	
	/*
	* Carica le rotte dal controller
	*/
	public function loadRouteFromController(){
		$this->loadController();
		
		$class_methods = get_class_methods(new $this->defaultController);
		
		if( $this->endsWith($this->defaultController, 'Controller') ){
			$url_controller = $this->removeEndString($this->defaultController, 'controller');
		}else{
			$url_controller = $this->defaultController;
		}
		
		foreach ($class_methods as $method_name) {
			
			if( $this->endsWith($method_name, 'Action') ){
				$actionName = $this->removeEndString($method_name, 'action');
				
				$route = new WB_Route( $url_controller.'/'.$actionName, array('action'=>$actionName) );
				
				$this->routes[] = $route;
			}
			
    		//echo "$method_name\n";
		}
		
		
		//$url, $default, $requirement=array(), $parameter= array()
		//
		
	}
	
	private function endsWith($str, $test){
		
		if( strlen( $test ) > strlen($str) ){return false;} 
		if(substr_compare( $str, $test, -strlen( $test ) ) === 0){
			return true;
		}else{
			return false;
		}
	}
	
	private function removeEndString( $str, $remove ){
		
		return substr($str,0,strlen($str)-strlen($remove) );
		
		
	}
	
	

}//end class

?>
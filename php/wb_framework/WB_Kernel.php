<?php
/*
 * Authore: Alessandro Carrer
 *
 * To do:
 * Version: 1.0
 *
 * Set WB_FRAMEWORK_DIR with the framework directory path
*/
class WB_Kernel{

	public $router;
	private $exceptionController;
	private $request;
	public $logger;
	private $wb_framework_dir;
	
	/* Costruttore */
	public function __construct() {
		try{
			if(isset($GLOBALS['WB_FRAMEWORK_DIR']) ){
				$this->wb_framework_dir = $GLOBALS['WB_FRAMEWORK_DIR'];
			}else{
				throw new Exception('Global variable WB_FRAMEWORK_DIR not initialize');
			}
			$this->init();			
		}catch (Exception $e) {
			if($this->logger != null){
				$this->logger->save();
			}
			$title = 'Errore nella inizializzazione del kernel';
			$this->printErrorMessage($title, $e->getMessage());
			die();
		}
	}

	/*
	* Inizialize all component
	*/
	private function init(){
		$dir = $this->wb_framework_dir;
		
		//Init logger
		$this->loadClass($dir.'WB_Logger.php');
		$this->logger = new WB_Logger($GLOBALS['WB_LOG_FILE']);
		$GLOBALS["WB_LOGGER"] = $this->logger;
		$GLOBALS["wb_logger"] = $this->logger;
		$this->logger->log('KERNEL', 'Inizializzato logger', '5');

		//Init Request
		$this->loadClass($dir.'WB_HttpRequest.php');
		$this->logger->log('KERNEL', 'Reading http request', '5');
		$this->request = new WB_HttpRequest();
		$this->logger->log('KERNEL', 'Serving'.$this->request->request_url , '5');

		//Init router
		$this->loadClass($dir.'WB_Route.php');
		$this->loadClass($dir.'WB_RouteCollection.php');
		$this->loadClass($dir.'WB_Router.php');		
		$this->router = new WB_Router();

		
		$this->loadClass($dir.'WB_HttpResponse.php');
		$this->loadClass($dir.'WB_Controller.php');
		
	}


	/**
	 * Get the kernel router
	 * @return kernel's router
	 */
	public function getRouter(){
		return $this->router;
	}

	
	public function getLogger(){
		return $this->logger;
		
	}


	/**
	 * Run the kernel
	 */
	public function run(){
		try{
			//$this->request = new WB_Request();
			$route = $this->router->getRequestRoute();
			if( $route == null){
				throw new Exception('Route does\'t exist');
			}

			// Load the correct controller funcion and
			$controllerFile = $route->getControllerPath();
			$controllerClassName = $route->getController();
			$action = $route->getAction();

			// Load class file
			$this->loadClass($route->getControllerPath());
			
			//Check if controller class exist
			if( class_exists($controllerClassName) ){
				$controller = new $controllerClassName();
				//$controller->setRequest( $this->request );
				
			}else{
				throw new Exception('Kernel class:'.$controllerClassName.' not found');
			}

			// Check if controller class method exist
			if(!method_exists ( $controller , $action )){
				throw new Exception('Controller method:'.$action.' not found');
			}


			$parameter = $route->getFunctionParameter();
			$route->setEnviromentParameter();
			
			
			
			// Call controller method
			call_user_func_array(array($controller, $action), $parameter);
			//$controller->$action();
			
			// Output controller result
			$controller->getResponse()->send();
			$this->logger->save();

		} catch (Exception $e) {
			$this->exceptionHandler($e);

		}
		finally{
			$this->logger->save();
			die();
		}

	}



	public function exceptionHandler( $exception){
		$e = $exception;
		//Flush all output buffer
		$wb_ob_level = ob_get_level();
		if ($wb_ob_level >= 1){
			for($i=1; $i<$wb_ob_level; $i++){
				ob_end_clean();
			}
		}
	
		$this->loadClass($this->exceptionController);
		$action = $e->getCode();
		$controller = new exceptionController();
		
		//Check if class method exist
		if(!method_exists ( $controller , $action )){
			$action = 'genericExceptionAction';
		}else{
			$title = 'Kernel error';
			$message = 'The class method for handling the exception does\'n exist';	
			$this->printErrorMessage($title, $message);
		}

		$controller->$action(
			$e->getCode(),
			$e->getMessage(),
			$e->getFile(),
			$e->getLine()
		);
		$controller->getResponse()->send();
		
	}
	
	
	/**
	 * Set the default exception controller
	 * @param file - Exception controller file path
	 */
	public function setExceptionController( $file ){
		$this->exceptionController = $file;

	}

	public function setFrameworkPath(){
		
		
	}
	
	
	/**
	 * Load a class file into the kernel
	 * @param file - class file path
	 */
	public function loadClass( $file ){
		if( file_exists($_SERVER['DOCUMENT_ROOT'].$file) ){
			require_once($_SERVER['DOCUMENT_ROOT'].$file);
		}else{
			throw new Exception('Kernel class file:'.$file.' not found');
		}

	}

	
	public function printErrorMessage( $title, $detail){
		echo '<div style="width: fit-content; 
			margin: auto; text-align: center; 
			border: 1px solid lightgrey; 
			border-radius: 5px; padding: 16px;">';
		echo '<p>'.$title.'</p>';
		echo '<p>'.$detail.'</p>';
		echo '</div>';

		
	}


}//end class
?>

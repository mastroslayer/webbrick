<?php
/*
 * Author: Alessandro Carrer
 *
 * To do:
 *
 *
 * Version: 0.1
*/
class WB_Router{

	private $path; //percorso richiesto

	private $pathTokens; //token percorso

	private $routes; //collezione di route
	
	private $routesPaths;

	private $routeCollections;
	
	/* Costruttore */
	public function __construct() {
		$this->routes = array();
		$this->path = array();
		$this->pathTokens = array();
		$this->routesPaths;
		$this->routeCollections = array();
	}



	/* Legge url dalla richiesta server */
	public function readURL(){
		//echo $_SERVER["REQUEST_URI"];

		$this->path=strtok($_SERVER["REQUEST_URI"],'?');

		$token = strtok($this->path, "/");
		while ($token !== false){
			$this->pathTokens[] = $token;
			$token = strtok("/");
		}
	}



	/* ----------------------------------------------------
	* Add a route
	* @param wb_route - Route
	*/
	public function addRoute( $wb_route ){
		$this->routes[] = $wb_route;
	}



	/*
	* Add a route collection
	* @param $wb_routecollection - Route collection
	*/
	public function addRouteCollection( $wb_routecollection ){
		$this->routeCollections[] = $wb_routecollection;
		$collection = $wb_routecollection->getRoutes();

		foreach( $collection as $route){
			$this->routes[] = $route;
			//echo '<br>Stampo rout inserita in router<br>';

			//$route->printDebug();
		}
		//echo '<br>Stampo collezione router<br>';
		//print_r( $collection );
	}


	
	/*
	* Carica
	*/
	public function loadRoutesFromDir( $path ){
		if( !file_exists( $path ) ){
			throw new Exception("Cannot load routes directory", 1);
		}
		$dir_files = array_diff(scandir($path), array('..', '.'));
		foreach( $dir_files as $file){
			if( $this->endsWith($file, 'Routes.php') | $this->endsWith($file, 'routes.php') ){
				include $path.$file;
			}
		}
	}
	
	private function endsWith($str, $test){
		if(substr_compare( $str, $test, -strlen( $test ) ) === 0){
			return true;
		}else{
			return false;
		}
	}
	

	/*
	* Select the requested route
	* @return Route
	*/
	public function getRequestRoute(){
		//print_r($this->routes);
		$this->readURL();
		$i=0;
		$count = count($this->routes);
		$found = false;
		$route = null;
		while( $i<count($this->routes) && $found==false ){

			$route = $this->routes[$i];

			if( $route->match( $this->pathTokens ) ){
				$found = true;
			}else{
				$found = false;
			}
			$i++;
		}

		if($found){
			return $route;
		}else{
			return null;
		}

	}

	
	/* Save the route collection to file 
	*
	*/
	public function saveRouteCollection($fileName){
		$myfile = fopen($fileName, "w");
		$routeCollectionName = "provaCollection";
			//or die("Unable to open file!");
		$txt = "<?php \n";
		$txt .= '$errorRoutes = new WB_RouteCollection();'."\n";
		foreach($this->routeCollections as $routeCollection){
			$txt .= $routeCollection->getFileString();
		}
			
			
		$txt .= '$this->addRouteCollection($errorRoutes);'."\n";
		$txt .= '?>'."\n";
		fwrite($myfile, $txt);
		fclose($myfile);
	}
		
	


	/*
	public function printRoute(){
		foreach($this->pathTokens as $token){
			echo 'Token:'.$token.'<br>';
		}

	}*/

	public function getRoutes(){
		return $this->routes;
		
	}
	
	public function getRouteCollections(){
		return $this->routeCollections;
		
	}
	
	public function printRoutes(){
		foreach($this->routes as $route){
			$route->printDebug();
		}

	}


}//end class
?>

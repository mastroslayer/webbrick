<?php
/*
 * Authore: Alessandro Carrer
 * 
 *
 * Class: WB_Controller
 *
 *
 * To do:
 * Gestione errori
 Version: 0.1
*/
class WB_Auth{
	
	/*
	0 Guest
	1 Register
	2 Manteiner
	3 Admin
	*/
	private $user_level;
	
	private $user_name;

	private $url_login;
	private $url_logout;
	private $url_login_fail;
	
	public function __construct() {
		if (session_status() == PHP_SESSION_NONE) {
    		session_start();
		}
		$this->readSession();
		
	}

	
	private function writeSession(){
		$_SESSION['wb_auth_user_level'] = $this->user_level;
		$_SESSION['wb_auth_user_name'] = $this->user_name;
	}
	
	private function readSession(){
		if( isset( $_SESSION['wb_auth_user_level'] ) ){ 
			$this->user_level = $_SESSION['wb_auth_user_level'];
		}else{
			$this->user_level = 0;
		}
		
		if( isset( $_SESSION['wb_auth_user_name'] ) ){ 
			$this->user_name = $_SESSION['wb_auth_user_name'];
		}else{
			$this->user_name = 'guest';
		}
	}
	
	
	public function getLevel( ){
		return $this->user_level;
	}

	public function getUserLevel( ){
		return $this->user_level;
	}
	
	public function setLevel( $level ){
		$this->user_level = $level;
	}
	
	public function setLoginUrl( $url ){
		$this->url_login = $url;
		
	}

	
	public function login( $name, $password){
		$login_status = false;
		
		if($name == 'admin' && $password=='betadminpsw'){
			$login_status = true;
			$this->user_level = 3;
			$this->user_name = 'admin';
			}
		if($login_status){
			//session_start();
			$this->writeSession();
			/* Session variables */
			
			return true;
		}else{
			return false;
		}

	}
	
	
	public function logout(){
		//session_start();
		// remove all session variables
		session_unset(); 
		// destroy the session 
		session_destroy();
		
	}

	
}//fine classe
?>
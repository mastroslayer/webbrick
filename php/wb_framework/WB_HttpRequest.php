<?php
/*
 * Authore: Alessandro Carrer
 *
 * To do:
 *
 * Version: 0.3
*/
class WB_HttpRequest{

	public $content_type; //array( 'MIMEType'=>'text', 'MIMESubtype'=>'html', 'quality'=>'0.5');
	public $content_language; //array('language'=>'it', 'country'=>'IT', 'quality'=>'0.9');
	public $content_encoding;
	public $request_url;
	public $request_url_tokens;
	private $debug;


	/* Costruttore */
	public function __construct( ) {
		$this->header_options = array();
		$this->content_language = array();
		$this->readRequest();
		$this->debug = false;
	}

	public function readRequest(){
		//$this->path=strtok($_SERVER["REQUEST_URI"],'?');
		$this->readRequestURL();
		//$this->content_language = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
		$this->readContentLanguage();
		$this->readContentType();
		$this->readContentEncoding();
		//$this->content_type = $_SERVER['HTTP_ACCEPT'];
		/*
		echo 'stampo';
		print_r( $this->content_language );
		print_r( $this->content_type );
		print_r( $this->content_encoding );*/
		//echo $this->content_type;
	}


	/* Legge url dalla richiesta server */
	public function readRequestURL(){
		$this->request_url=strtok($_SERVER["REQUEST_URI"],'?');
		$this->request_url_tokens=explode("/", $this->request_url);
	}
	
		
	public function readContentLanguage(){
		$tmp = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
		$tokens = explode(',',$tmp) ;
		foreach( $tokens as $tok){
			$this->content_language[] = $this->contentLanguageToArray($tok);
		}
	}


	private function contentLanguageToArray( $str ){

		$lang_country_str=strtok($str, ";");
		$quality_str=strtok(";");
		$quality = null;
		$country = null;
		//Get Quality
		if( $quality_str != null){
			strtok($quality_str, "=");
			$quality=strtok("=");
		}

		//Get Country and language
		if( $lang_country_str != null){
			$language = strtok($lang_country_str, "-");
			$country=strtok("-");
		}

		if( $quality == null ){$quality = '*';}
		if( $country == null ){$country = '*';}
		return array( 'language'=>$language, 'country'=>$country, 'quality'=>$quality);
	}


	public function readContentType(){
		$tmp = $_SERVER['HTTP_ACCEPT'];
		$tokens = explode(',',$tmp) ;
		foreach( $tokens as $tok){
			$this->content_type[] = $this->contentTypeToArray($tok);
		}
	}

	private function contentTypeToArray( $str ){

		$lang_country_str=strtok($str, ";");
		$quality_str=strtok(";");

		$quality = null;
		$country = null;
		//Get Quality
		if( $quality_str != null){
			strtok($quality_str, "=");
			$quality=strtok("=");
		}

		//Get Country and language
		if( $lang_country_str != null){
			$language = strtok($lang_country_str, "/");
			$country=strtok("/");
		}

		if( $quality == null ){$quality = '*';}
		if( $country == null ){$country = '*';}
		return array( 'MIMEType'=>$language, 'MIMESubtype'=>$country, 'quality'=>$quality);
	}


	public function readContentEncoding(){
		//echo 'Leggo content type';
		$tmp = $_SERVER['HTTP_ACCEPT_ENCODING'];
		//echo 'Type:'.$tmp.'<br>';
		$tokens = explode(',',$tmp) ;
		foreach( $tokens as $tok){
			$this->content_encoding[] = $tok;
			//echo '<br>';
		}

	}

	
	public function getGetData( $name, $alt=null ){
		if( array_key_exists($name, $_GET) ){
			return $_GET[$name];
		}else{
			return $alt;
		}
	}
	
	
	public function getQuery( $name, $alt=null){
		return $this->getGetData( $name, $alt );
	}
	
	
	public function getPostData( $name, $alt=null ){
		if( array_key_exists($name, $_POST) ){
			return $_POST[$name];
		}else{
			return $alt;
		}
	}



	public function getContentLanguage(){

	}

	public function getContentLanguages(){

	}

}//end class
?>

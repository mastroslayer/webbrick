<?php
/****************************************************
 * Autore: Alessandro Carrer
 * 
 * Classe:
 * 
****************************************************/


class TableField{


	public $name = null;
	public $type = 'VARCHAR';
	public $size = 0;
	public $unique = false;
	public $binary = false;
	public $unsigned = false;
	public $zero_fill = false;
	public $auto_increment = false;
	public $not_null = false;
	public $default = null;
	public $comment = null;
	public $primary_key = false;
	
	public $name_encripted = null;
	
	public function __construct($name, $data=null) {
		$this->name = $name;
	}



    function get_type_sql(){
        $type = null;
        $res = 'ERROR';

        switch (strtoupper($this->type)) {
            case 'INT':
                $res =  'INT';
                break;

            case 'DOUBLE':
                $res =  'DOUBLE';
                break;

            case 'VARCHAR':
                $res =  'VARCHAR('.$this->size.')';
                break;

            case 'DATETIME':
                $res =  'DATETIME';
                break;
            case 'DATE':
                $res =  'DATE';
                break;
            case 'LONGTEXT':
                $res =  'LONGTEXT';
                break;
            default:
                $res =  'ERROR';
        }

        return $res;
    }

	
	
function get_mysql_options(){
		$type = null;
		$str = 'ERROR';

		switch (strtoupper($this->type)) {
			case 'INT':
				$str =  'INT';
				if($this->unsigned == true){ $str .= ' UNSIGNED';}
				if($this->not_null == true){ $str .= ' NOT NULL';}
				if($this->auto_increment == true){ $str .= ' AUTO_INCREMENT';}
				if($this->default != null){ 
					$str .= ' DEFAULT';
					$str .= ' '.$this->default;	
				}
				break;
				
			case 'DOUBLE':
				$str =  'DOUBLE';
				break;
				
			case 'VARCHAR':
				$str =  'VARCHAR('.$this->size.')';

				if($this->not_null == true){ $str .= ' NOT NULL';}
				if($this->default != null){ 
					$str .= ' DEFAULT';
					$str .= ' \''.$this->default.'\'';	
				}
				break;
				
			case 'DATETIME':
				$str =  'DATETIME';
				break;
			case 'DATE':
				$str =  'DATE';
				break;
			case 'LONGTEXT':
				$str =  'LONGTEXT';
				break;
			default:
				$str =  'ERROR';
		}

	return $str;
	
}	
	
	
}// fine classe



?>

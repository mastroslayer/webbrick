<?php
/**
 * La classe rappresenta la struttura dati di una tabella
 * la classe puo generare il codice sql di
 *
 * calyw: sono caduta adosso a me stessa
 *
 *
 * TODO:
 *
 * -implementare update e delete con arrya in paramaetro
 *
 *
 */

class Table {

	// Table name
	public $name = null;

	// Field name
	public $field = null;

	public $field_encripted;
	
	// Table primary key
	public $primary_key = array();

	// Foreign key
	public $foreign_key = null;
	//array('column'=>$fk_col, 'reference_table'=>$fk_ref_table, 'reference_column'=>$fk_ref_col);
	// Table charset
	public $charset='utf8';

	public $indexes = array();
	// Table engine
	public $engine='InnoDB';

	public $debug = true;		// Abilita il debug
	public $debug_level = 0;	// Livello di debug 4:Critical 2:Error 2:Warning 1:Info 0:debug off



	public function __construct($name, $tbl_struct=null) {
		$field = array();
		$field_encripted = array();
		$this->name = $name;
		$this->db_link = null;
		$this->foreign_key = array();
		$this->primary_key = array();
	}



	public function add_field( $field ){
				
		$this->field[$field['name']] = new TableField($field['name'], $field);
		if (array_key_exists('type', $field)){ $this->field[$field['name']]->type = $field['type'];}
		if (array_key_exists('size', $field)){ $this->field[$field['name']]->size = $field['size'];}
		if (array_key_exists('unique', $field)){ $this->field[$field['name']]->unique = $field['unique'];}
		if (array_key_exists('auto_increment', $field)){ $this->field[$field['name']]->auto_increment = $field['auto_increment'];}
		if (array_key_exists('not_null', $field)){ $this->field[$field['name']]->not_null = $field['not_null'];}
		if (array_key_exists('default', $field)){ $this->field[$field['name']]->default = $field['default'];}
		if (array_key_exists('primary_key', $field)){ $this->field[$field['name']]->primary_key = $field['primary_key'];}
		if( $this->field[$field['name']]->primary_key ){ $this->primary_key[] = $field['name']; }
		$this->field[$field['name']]->name_encripted = $this->encriptString($field['name']);
		
		$this->field_encripted[$this->encriptString($field['name'])] = $field['name'];
	}



	public function get_field($field_name){
		if (array_key_exists($field_name, $this->field)){
			return $this->field[$field_name];
		}else{return null;}
	}

	
	public function encriptString($string){
		return sha1($string);
		
	}
	
	public function getFieldName($field_name){
		if( $WB_DATABASE_FIELD_ENCRIPTION ){
			return $this->encriptString($field_name);
		}else{
			return $field_name;
		}	
	}

	
	public function printFieldName($field_name){
		if( $GLOBALS['WB_DATABASE_FIELD_ENCRIPTION'] ){
			echo $this->encriptString($field_name);
		}else{
			echo $field_name;
		}	
	}	
	
	/* --------------------------------------------------------------------
	 * Function: fieldExist()
	 * Check if a field exist
	 * @param $fieldName
	 * @return if
	 * ------------------------------------------------------------------*/
	public function fieldExist($field_name){
		if (array_key_exists($field_name, $this->field)){
			return true;
		}else{
			return false;
		}

	}



	/* --------------------------------------------------------------------
	 * Function: getFields()
	 * Check if a field exist
	 * @return array of all field object
	 * ------------------------------------------------------------------*/
	public function getFields(){
		return $this->field;
	}

	public function getEncriptedFields(){
		return $this->field_encripted;
	}
	
	public function getFieldList(){
		$list = array();
		foreach($this->field as $campo){
			$list[] = $campo->name;
		}
		return $list;
	}
	

	public function getEncriptedFieldList(){
		$list = array();
		foreach($this->field as $campo){
			$list[] = $campo->name_encripted;
		}
		return $list;
	}	
	
	public function decriptFields($prm){
		$parametri = array();
		
	
		foreach($prm as $key => $value){
			if( array_key_exists( $key, $this->field_encripted) ){
				echo 'trovato';
				$parametri[ $this->field_encripted[$key] ] = $value;
			}else{
				echo 'non trovato';
				
			}
		}
		
		return $parametri;
	
	}
	
	/* --------------------------------------------------------------------
	 * Function: getFields()
	 * Check if a field exist
	 * @return array of all field object
	 * ------------------------------------------------------------------*/
	public function add_foreign_key( $fk_name, $fk_col, $fk_ref_table, $fk_ref_col){
		$this->foreign_key[$fk_name] = array('column'=>$fk_col, 'referenced_table'=>$fk_ref_table, 'referenced_column'=>$fk_ref_col);
	}

	public function addForeignKey( $fk_name, $fk_col, $fk_ref_table, $fk_ref_col, $fk_on_update='NO ACTION', $fk_on_delete='NO ACTION'){
		$this->foreign_key[$fk_name] = array('column'=>$fk_col, 'referenced_table'=>$fk_ref_table, 'referenced_column'=>$fk_ref_col, 'on_update'=>$fk_on_update, 'on_delete'=>$fk_on_delete);
	}



	/* --------------------------------------------------------------------
	 * Crea una query di aggiornamento, controlla che i parametri passati
	 * corrispondano ai campi
	 * Aggiorna un record leggendo i parametri inviati tramite GET o POST
	 * @param metodo POST o GET
	-------------------------------------------------------------------- */
public function update($prm){
	$parametri = $prm;

	$pk = null;
	$QStr = "UPDATE ".$this->name.' SET ';

	$tmpI=0;
	foreach($parametri as $key=>$value){

		if($key == $this->primary_key[0]){
			$pk = $value;
			$updatedID = $value;
		}else{
			if($tmpI>0){$QStr .=', ';}
			$QStr .= $key.'=:'.$key;
			$tmpI++;
		}

	}

	$QStr .= ' WHERE '.$this->primary_key[0]."=:".$this->primary_key[0];
	if($this->debug){echo $QStr;}
	return $QStr;



}





/* -----------------------------------------------------
* Inserisce un record leggendo i parametri inviati tramite GET o POST oppure
* passando un array
* @param metodo POST o GET
* @param $prm array di parametri
-------------------------------------------------------- */
public function insert($prm){
	if( ($prm == null) | (!is_array($prm)) ){
		throw new Exception("WBTable:: errore lettura parametri", 1);
	}

	$parametri  = $prm;
		
	
	
	//$parametri = $this->read_POST();
	$pk = null;
	$QStr = "INSERT INTO ".$this->name.' ( ';

	$tmpI=0;
	foreach($parametri as $key => $value){

		if($key == $this->primary_key[0]){
			$pk = $value;
		}else{
			if($tmpI>0){$QStr .=', ';}
			$QStr .= $key;
			$tmpI++;
		}
	}

	$QStr .= ') VALUE ( ';

	$tmpI=0;
	foreach($parametri as $key => $value){
		if($key == $this->primary_key){
		}else{
			if($tmpI>0){$QStr .=', ';}
			$QStr .= ':'.$key;
			$tmpI++;
		}
	}
	$QStr .= ' ) ';
	echo $QStr;
	return $QStr;
}





/* -----------------------------------------------------
* Rimuove un record leggendo l'id tramite post o get
* @param metodo POST o GET
-------------------------------------------------------- */
public function delete(){
	$pk = $this->primary_key[0];
	$QStr = "DELETE FROM ".$this->name.' WHERE ';
	$QStr .= $pk.'=:'.$pk;
	return $QStr;
}



public function drop(){
	$sql = "DROP TABLE IF EXISTS ".$this->name;
	return $sql;
}




/* -----------------------------------------------------
 * Crea tabella
 * @param metodo POST o GET
 *
-------------------------------------------------------- */
public function create(){

	//echo 'inizio';
	$format = false;
	$pk = null;
	//print_r($this->field);
	try {

		$i = 0;
		// use exec() because no results are returned
		$sql = "CREATE TABLE IF NOT EXISTS ".$this->name." (";
		foreach($this->field as $field){

			if($i > 0){$sql .= ', ';}

			if($format){$sql .= '<br>';}
			$sql .= $field->name." ";
			//$sql .= $field->get_type_sql();
			$sql .= $field->get_mysql_options();
			$i++;
		}
		if($i > 0){$sql .= ',';}
		$sql .= " PRIMARY KEY (";
		foreach($this->primary_key as $pk){
			$sql .= $pk;
		}
		$sql.= ")";

		// Genera indici
		foreach($this->indexes as $idx){

		}

		// Genera indici automatici foreign key
		foreach($this->foreign_key as $fk){
			$sql .= ' ,';
			$sql .= ' INDEX '.'fk_'.$fk['column'].'_idx ( '.$fk['column'].' ASC)';

		}

		//echo 'ci sono';
		// Foreign key
		foreach($this->foreign_key as $fk_name => $fk){
			/*echo 'Stampo contenuto:';
			print_r($fk);*/
			$sql .= ' ,';
			$sql .= ' CONSTRAINT '.$fk_name;
			$sql .= ' FOREIGN KEY ('.$fk['column'].') ';
			$sql .= ' REFERENCES '.$fk['referenced_table'].'('.$fk['referenced_column'].')';
			$sql .= ' ON UPDATE '.$fk['on_update'];
			$sql .= ' ON DELETE '.$fk['on_delete'];
		}

		$sql .= " ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		return $sql;
		//echo $sql.'<br>';
		//$this->db_link->exec($sql);
		//echo "Tabella ".$this->tbl_struct->name." creata con successo<br>";
	}catch(PDOException $e){
		echo $sql . "<br>" . $e->getMessage().'<br>';
	}catch(Exception $e){
		echo "Error while generating sql code";
	}
}


/* -----------------------------------------------------
 * Restituisce i valori di default
 *
 *
-------------------------------------------------------- */
public function getDefaultValues(){
	$res = array();
	$t = array();
	foreach($this->field as $field ){
		$res[$field->name] = $field->default;
	}
	$t[] = $res;
	//print_r( $res );
	return $t;
}





/* -----------------------------------------------------
 * Crea tabella
 * @param metodo POST o GET
-------------------------------------------------------- */
public function selectRecord(  ){

	$format = false;
	$pk = null;
	$i = 0;
	// use exec() because no results are returned
	$sql = "SELECT  * FROM ".$this->name." ";
	$sql .= 'WHERE '.$this->primary_key[0].'=:'.$this->primary_key[0];
	return $sql;

}


/* Legge tutta la tabella

*/
public function read(){
	//$query_str = "SELECT * FROM"


}


public function size(){
	$QStr = 'SELECT COUNT(*) as size FROM '.$this->tbl_struct->name;
	$Stmt->execute();

}



}//Fine classe
?>

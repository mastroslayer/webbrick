<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/php/wb_database/WB_TableField.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/wb_database/WB_Table.php');

class WB_Database{

	public $name = null;
	public $db_link = null;
	public $host = 'localhost';
	public $type = 'mysql';
	public $username = 'username';
	public $password = 'password';
	public $debug = false;
	// List of table object
	public $table = null;
	private $logger;






	public function __construct($type, $host, $name, $username, $password) {
		$this->type = $type;
		$this->host = $host;
		$this->name = $name;
		$this->username = $username;
		$this->password = $password;
		$this->table = array();
		$this->db_link = null;
		$this->debug=false;
		$this->logger = $GLOBALS["WB_LOGGER"];
	}



	public function connect(){
		$CONF_DB_STRING = $this->type.':host='.$this->host.';dbname='.$this->name;
		try{
			//echo 'connetto...';
			$this->logger->log('wb_database', 'Connessione al database', 3);
			$this->db_link = new PDO($CONF_DB_STRING, $this->username, $this->password);
			$this->db_link->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch (PDOException $e) {
			$this->logger->log('wb_database', 'Errore di connessione al database', 0);
			$this->logger->log('wb_database', $CONF_DB_STRING, 3);
			$this->logger->log('wb_database', $e->getMessage(), 3);
			$this->db_link = null;
		}
	}

	
	
	public function disconnect(){
		$this->db_link = null;
	}



    /*-------------------------------------------------------------------------
    * Aggiunge una tabella a partire da un array associativo
    * che descrive la tabella
    * @param string $tbl_name nome della tabella da inserire
    * @param array $tbl_struct array associativo che descrive la tabella
    */
	function addTable($tbl_name, $tbl_struct){
		$this->table[$tbl_name] = new Table($tbl_name);
		foreach($tbl_struct as $row){
			$this->table[$tbl_name]->add_field($row);
		}

	}



    /*
    * Aggiunge le tabelle a partire da un array associativo che
	* descrive il database
    * @param string $db_struct nome della tabella da inserire
    * @param array $tbl_struct array associativo che descrive la tabella
    */
	function setStructure( $db_struct ){
		foreach($db_struct as $tbl_name=>$tbl_struct){
			//key($tbl_struct);

			//echo 'inserisco tabella:'.$tbl_name.'<br>';
			$this->addTable($tbl_name, $tbl_struct['field']);

			if( array_key_exists('foreign_keys', $tbl_struct) ){
				foreach($tbl_struct['foreign_keys'] as $fk){
					if( !array_key_exists('on_update', $fk) ){ $fk['on_update'] = 'NO ACTION'; }
					if( !array_key_exists('on_delete', $fk) ){ $fk['on_delete'] = 'NO ACTION'; }

					//print_r($fk);
					$this->table[$tbl_name]->addForeignKey( $fk['name'], $fk['column'], $fk['referenced_table'], $fk['referenced_column'], $fk['on_update'], $fk['on_delete'] );
				}
			}
		}

	}




/*
 * Restituisce la classe della tabella specificata
 * @param string $tbl_name nome tabella
 * @return restituisce la classe tabella altrimenti restituisce null
 */
	function getTable($tbl_name){
        if (array_key_exists($tbl_name, $this->table)){
            return $this->table[$tbl_name];
        }
        else{
			return null;}
	}
/*
!!!!!! Deprecate in eliminazione
*/
	function get_table($tbl_name){
		//$this->getTable($tbl_name);
        if (array_key_exists($tbl_name, $this->table)){
            return $this->table[$tbl_name];
        }
        else{
			return null;}
	}




    /*
    * Restituisce la classe della tabella specificata
    * @param void
    * @return array restituisce un array di stringe contenenti il nome delle tabelle
    */
	function get_table_list(){
        $tmp = array();
        foreach($this->table as $tbl){
            $tmp[] = $tbl->name;
        }
        return $tmp;

	}

	function getTables(){
        return $this->table;

	}


    /*
    function printHtml_table_struct($tbl_name){
        echo "<table>";
        for ($i=0; $i<count($this->table[$tbl_name]->field); $i++){

            echo "<tr><td>".$this->table[$tbl_name]->field[$i]."</td></tr>";

        }
        echo "</table>";
    }*/




    function printHtml_table_list(){
        echo count($this->table);
        $keys = array_keys($this->table);
        //print_r($keys);
        echo "<table>";
        for ($i=0; $i<count($keys); $i++){

            echo "<tr><td>".$this->table[$keys[$i]]->name."</td></tr>";

        }
        echo "</table>";
    }


    /* -----------------------------------------------------------------------------------
	Esegue una query nella tabella
	@param query_string - string - Specifica la query da eseguire
	@param param - array - Array associativo chiave valore per il bind dei parametri
  ----------------------------------------------------------------------------------*/
public function query($query_string, $param_array=null, $type='SELECT'){
		$query_stmt = null;
		$this->logger->log('wb_database', 'Execute query', 3);
		$this->logger->log('wb_database', $query_string, 3);
		$stopwatch = new WB_Stopwatch();
	$stopwatch->start('evvai');
		if($query_string != null){

			$query_stmt = $this->db_link->prepare($query_string);
			if(sizeof($param_array)>0){
				foreach($param_array as $key=>$value){
					//$this->query_stmt->bindParam($key, $value);
					$query_stmt->bindValue($key, $value);
					//echo 'Key='.$key.'Value='.$value;
				}
			}
			//esegue query

			$iterator = null;
			try{
				$query_stmt->execute();
				$iterator = new WB_Dataset();
				if($type == 'SELECT'){
					$query_stmt->setFetchMode(PDO::FETCH_ASSOC);
					$iterator = new WB_Dataset($query_stmt->fetchAll());
				}
				$query_stmt = null;
					$stopwatch->stop();
            	return $iterator;
			}catch(PDOException $e) {
				if($this->debug){
					$this->logger->log('wb_database', 'Query error', 2);

					echo 'Errore nella query:'.$query_string;
				}
				$query_stmt = null;
				$iterator = new WB_Dataset(array('Message'=>'Error'));
        		return $iterator;
    		}finally{
				return $iterator;
			}
			//$this->query_result_count = $this->query_stmt->rowCount();
			//$this->query_result = $this->query_stmt->fetchAll();
			//$this->query_index = 0;
		}
}



/* Utilizzata solo dalle classi interne
 *
 *
*/
public function bind( $statment, $parametri ){
		if( $parametri == null ){
			$this->logger->log('wb_database', 'No parameter to bind for query', 2);

			throw new Exception("WBDatabase::bind Nessun parametro per il bind", 1);
		}
		if($this->debug){
			echo '<br>';
		}
		//print_r($parametri);
		// Bind parametri
		foreach($parametri as $key => $value){
			$this->logger->log('wb_database', 'Binding parameter:'.$key.':'.$value , 3);

			if($this->debug){

				echo '---------- Bind parametro:';
				echo $key.': '.$value."<br>";
			}
			try{
				//todo controllo vaidita tipo di dato


			  $statment->bindValue(':'.$key, $value); // use bindParam to bind the variable
			}catch (PDOException $e) {
				$this->logger->log('wb_database', 'Error binding value on query:' , 1);
				$this->logger->log('wb_database', $e->getMessage() , 3);

				echo 'Errore di esecuzione';
				echo $e->getMessage();
			}
		}

}



public function updateRecord( $table_name, $parametri ){
		$this->logger->log('wb_database', 'Execute update query', 3);
		$this->logger->log('wb_database', 'Updating table '.$table_name, 3);


		if($this->debug){
			echo 'Aggiorno record tabella:'.$table_name.'<br>';
			print_r($parametri);
		}
		$query_string = $this->getTable($table_name)->update($parametri);
		if($this->debug){
			echo $query_string;
		}
		if( $query_string == null){ return null;}

		$Stmt = $this->db_link->prepare($query_string);
		//$this->bind($Stmt, array($this->getTable($table_name)->primary_key[0]=>$id));
		$this->bind($Stmt, $parametri);

		$updatedID = null;
		// Eseguo query di aggiornaento
		try{
			if($this->debug){ echo "Model: Eseguo query di aggiornamento <br>";}
			$Stmt->execute();
		}
		catch (PDOException $e) {
			echo '<br>Errore di esecuzione ';
			echo $e->getMessage();
		}
		finally{
			return $updatedID;
		}

}




public function selectRecord( $table_name, $id ){
		//$this->connect();
		//echo 'Aggiorno record';
		$query_string = $this->getTable($table_name)->selectRecord();

		$pk = $this->getTable($table_name)->primary_key[0];

		if( $query_string == null){ return null;}

		$Stmt = $this->db_link->prepare($query_string);

		$this->bind($Stmt, array($pk=>$id));

		$iterator = null;

		// Eseguo query di aggiornaento
		try{
			if($this->debug){ echo "Model: Eseguo query di selezione record <br>";}
			$Stmt->execute();
			$Stmt->setFetchMode(PDO::FETCH_ASSOC);
			$iterator = new WB_Dataset($Stmt->fetchAll());
			return $iterator;
		}
		catch (PDOException $e) {
			echo 'Errore di esecuzione';
			echo $e->getMessage();
		}
		finally{
			return $iterator;
		}

}




public function deleteRecord( $table_name, $id ){
		//$this->connect();
		//echo 'Aggiorno record';
		$query_string = $this->getTable($table_name)->delete();

		$pk = $this->getTable($table_name)->primary_key[0];

		if( $query_string == null){ return null;}

		$Stmt = $this->db_link->prepare($query_string);

		$this->bind($Stmt, array($pk=>$id));

		$iterator = null;

		// Eseguo query di eliminazione
		try{
			if($this->debug){ echo "Model: Eseguo query di eliminazione record <br>";}
			$iterator = $Stmt->execute();

			//$Stmt->setFetchMode(PDO::FETCH_ASSOC);
			//$iterator = new QueryIterator($Stmt->fetchAll());
			return $iterator;
		}
		catch (PDOException $e) {
			echo 'Errore di esecuzione';
			echo $e->getMessage();
		}
		finally{
			return $iterator;
		}

}


public function getDefaultValues( $table_name ){
	return $this->getTable( $table_name )->getDefaultValues();

}


public function insertRecord( $table_name, $parametri ){
		//$this->connect();
		$this->logger->log('wb_database', 'Query insert' , 3);

		$query_string = $this->getTable($table_name)->insert($parametri);
		$this->logger->log('wb_database', $query_string , 3);

		if( $query_string == null){ return null;}

		$Stmt = $this->db_link->prepare($query_string);
		$this->bind($Stmt, $parametri);

		$ID = null;
		// Eseguo query di aggiornaento
		try{
			if($this->debug){ echo "Model: Eseguo query di aggiornamento <br>";}
			$Stmt->execute();
			$ID = $this->db_link->lastInsertId();
		}
		catch (PDOException $e) {
			echo 'Errore di esecuzione';
			echo $e->getMessage();
		}
		finally{
			return $ID;
		}

}









public function resolve_dependencies(){
	$create_list=array();
	foreach($this->table as $tbl){
		echo 'Elaboro: '.$tbl->name.'<br>';
		// tabella ha dipendenza
		if( count($tbl->foreign_key) > 0 ){
			foreach( $tbl->foreign_key as $fk ){

				// dipendenza non in elenco
				if( !in_array( $fk['referenced_table'], $create_list )){
					echo 'Dipendenza trovata, inserisco dipendenza '.$fk['referenced_table'].'<br>';
					if( array_key_exists($fk['referenced_table'], $this->table) ){
						echo 'Tabella Dipendenza esiste'.'<br>';
						$create_list[]=$fk['referenced_table'];
					}else{
						echo 'Tabella Dipendenza non esiste'.'<br>';
						throw new Exception('Impossibile risolvere dipendenze di '
							.$tbl->name.' referenced '
							.$fk['referenced_table'].' non esiste'
						);
					}

				}

			}

			if( !in_array($tbl->name, $create_list) ){
				echo 'fine dipendenze, inserisco '.$tbl->name.'<br>';
				$create_list[]=$tbl->name;
			}
		}else{
			if( !in_array($tbl->name, $create_list) ){
				echo 'Nessuna dipendenza: inserisco '.$tbl->name.'<br>';
				$create_list[]=$tbl->name;
			}
		}

	}
	return $create_list;
}


public function create_database_structure(){
	$tbl_list = $this->generate_create_list();
	foreach($tbl_list as $tbl){
		$this->create_table($tbl);
	}
}


public function delete_database_structure(){
	$tmp = $this->generate_create_list();
	$tbl_list = array_reverse ( $tmp );
	foreach($tbl_list as $tbl){
		$this->delete_table($tbl);
	}
}


private function generate_create_list(){
	$create_list=array();
	foreach($this->table as $tbl){
		$tmp_list = $this->resolve_dependencies_tree($tbl->name);
		foreach ($tmp_list as $tmp) {
			if( !in_array($tmp, $create_list) ){ $create_list[] = $tmp; }
		}

	}
	return $create_list;
}

private function resolve_dependencies_tree( $table_name ){
	$dep_list = array();

	if( !array_key_exists($table_name, $this->table) ){
		throw new Exception( 'Cannot solve dependencies of '.$table_name );
	}
	$tbl = $this->table[$table_name];
	if( count($tbl->foreign_key) < 1 ){
		$dep_list[]=$table_name;
		return $dep_list;
	}

	foreach ($tbl->foreign_key as $fk) {
		$tmp_list = $this->resolve_dependencies_tree( $fk['referenced_table'] );
		foreach ($tmp_list as $tmp) {
			if( !in_array($tmp, $dep_list) ){ $dep_list[] = $tmp; }
		}
	}
	if( !in_array($table_name, $dep_list) ){ $dep_list[] = $table_name; }
	return $dep_list;
}




public function create_table( $tbl_name ){
	$sql_code = $this->getTable($tbl_name)->create();
	//echo $sql_code;
	$this->db_link->exec($sql_code);
}

public function delete_table( $tbl_name ){
	$sql_code = $this->getTable($tbl_name)->drop();
	$this->db_link->exec($sql_code);
}

}/* Fine Classe */

?>

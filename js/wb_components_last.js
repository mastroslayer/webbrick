/*
*
* Esempio definizione eventi
var add_button = {
	url:'fatture/vendita/new',
	on_close:function(){wb_modal_close();},
	on_confirm:function(){document.getElementById('form_modal').submit();},
	on_deny:function(){wb_modal_close();}
}

var my_modal = new wb_modal( );
my_modal.set_config( config_edit_cliente );	
my_modal.addEvent( 'click', 'myDomElement', function(){ modal_cliente.open(); } );	
*
*
*
*
* Elementi modal window
* -------------------------------------------------------
* wb-modal-window-button-close: Pulsante chiudi finestra
* wb-modal-window-button-confirm: Pulsante ok/salva
* wb-modal-window-button-deny: Pulsante annulla
* wb-modal-window-title: Titolo della finestra
* wb-modal-window-text
* wb-modal-window-form
*
* wb-modal-window-body
* wb-modal-window-footer
*
*/


/*
* Gestione tabelle
* Esempio definizione eventi
*
*/

/*
*
*/
	function wbTableDisplayColumn( tableId, column, state ){
		
		var tbl  = document.getElementById(tableId);
		var rows = tbl.getElementsByTagName('tr');

		var cels = rows[0].getElementsByTagName('th');
		cels[column].style.display=state;
		for (var row=1; row<rows.length;row++) {
		  var cels = rows[row].getElementsByTagName('td');
		  cels[column].style.display=state;
		}
		
	}







/* 
*
* Esempio definizione eventi
var add_button = {
	url:'fatture/vendita/new',
	on_close:function(){wb_modal_close();},
	on_confirm:function(){document.getElementById('form_modal').submit();},
	on_deny:function(){wb_modal_close();}
}

var my_modal = new wb_modal( );
my_modal.set_config( config_edit_cliente );	
my_modal.addEvent( 'click', 'myDomElement', function(){ modal_cliente.open(); } );	
*
*
*
*
* Elementi modal window
* -------------------------------------------------------
* wb-modal-window-button-close: Pulsante chiudi finestra
* wb-modal-window-button-confirm: Pulsante ok/salva
* wb-modal-window-button-deny: Pulsante annulla
* wb-modal-window-title
* wb-modal-window-text
* wb-modal-window-form
*
*/




class WBTable {


	constructor( ) { 
		//this.name = name; 
		this.url = null;
		this.show_window_after_ajax=false;
		this.tableId='table_name';
		this.formActionId = null;
		this.formActionFieldName = 'id';
		this.formInputIdPrefix = 'prefix';
		
		this.selectedItemsCount = 0 ;
	}
	
	onSelection( checkboxId ){
		var formAction = document.getElementById(this.formActionId);
		var checkbox = document.getElementById( checkboxId );
		
		if( checkbox.checked == true ){
			var inputField = document.createElement("INPUT");
			inputField.setAttribute("type", "text");
			inputField.setAttribute("name", this.formActionFieldName+'[]');
			inputField.setAttribute("value", checkbox.value);
			inputField.setAttribute("id", this.formInputIdPrefix+checkbox.value);
			formAction.appendChild(inputField);
			this.selectedItemsCount ++;
		}else{
			inputField = document.getElementById(this.formInputIdPrefix+checkbox.value);
			if( inputField != null){
				formAction.removeChild(inputField);
				this.selectedItemsCount --;
			}
		}
	}
	
	addRow( data ){
		var html_row = '<tr>';
		
		html_row += '<td>'+data.produttore+'</td>';
		
		html_row += '</tr>';
		//var html_row = '<tr><td>AAAA</td></tr>';
		var table = document.getElementById(this.tableId);
		//var table_body = table.getElementsByTagName('tbody');
		table.innerHTML += html_row ;
		
	}
	
	removeRow( rowId ){
		
	}
	
	updateRow(){
		
	}
	
	setData(){
		
	}
	
	readJSON( jsondata ){
		var data = jsondata['data'];
		for( var i=0; data.length; i++ ){
			this.addRow( data[i] );
			
			
			
		}
		
	}
	
	
	
}


/* ==========================================================================================

*/


class WBModal{
	
	constructor( id=null ){
		this.modalId=id;
		
		this.data = null;
		
		this.formId = null;
		this.formAction = null;
		this.formDataUrl = null;
		
		this.ObjReference = this;
		
		this.title = 'Title';
		this.titleId = null;
		
		this.message = 'Message';
		
		this.buttonConfirmId = null;
		this.onConfirm = null;
		
		this.buttonDenyId = null;
		this.onDeny = null;
		
		this.windowStyle = 'alert';
		
	}
	
	

	
	
	
	setup( ){
		
		if(this.modalId != null){
		   
		}else{
			this.generate();
			this.generateSimpleModal();
			this.modalId = 'wb-modal-container';
			this.buttonConfirmId = 'wb-modal-window-button-confirm';
			this.buttonDenyId = 'wb-modal-window-button-deny';
		}
		
		// Modifica titolo finestra
		if( this.titleId != null){
			document.getElementById(this.titleId).innerHTML = this.title;
		}
		
		
		if( this.formId != null ){
			document.getElementById(this.formId).action = this.formAction;
		}
		
		this.attachEvents();
	}
	
	
	/* Genera la modal window vuota */
	generate() {
		var modal_container = document.createElement('div');
		modal_container.id = 'wb-modal-container';
		modal_container.style.display = 'none';
		document.body.appendChild(modal_container);
		
		var modal_background = document.createElement('div');
		modal_background.classList.add('wb-modal-background');
		modal_container.appendChild(modal_background);
		
		var modal_window = document.createElement('div');
		modal_window.id = 'wb-modal-window';
		modal_window.classList.add('wb-modal-window');
		modal_window.classList.add(this.windowStyle);
		modal_background.appendChild(modal_window);
		
		var modal_head = document.createElement('div');
		modal_head.id = 'wb-modal-window-head';
		modal_head.classList.add('head');
		modal_window.appendChild(modal_head);
		
		var modal_body = document.createElement('div');
		modal_body.id = 'wb-modal-window-body';
		modal_body.classList.add('body');
		modal_window.appendChild(modal_body);
		
		var modal_footer = document.createElement('div');
		modal_footer.id = 'wb-modal-window-footer';
		modal_footer.classList.add('footer');
		modal_window.appendChild(modal_footer);

	}
	
	generateSimpleModal(){
		var head_structure = "<span class='title'>" + this.title + "</span>";
		document.getElementById('wb-modal-window-head').innerHTML = head_structure;
		
		var body_structure = "<p class='message'>" + this.message + "</p>"
		document.getElementById('wb-modal-window-body').innerHTML = body_structure;
		
		
		/*var modal_button_group_left = document.createElement('div');
		modal_button_confirm.id = 'wb-modal-window-button-confirm';
		*/
		var modal_button_confirm = document.createElement('button');
		modal_button_confirm.id = 'wb-modal-window-button-confirm';
		modal_button_confirm.innerHTML = 'elimina';
		document.getElementById('wb-modal-window-footer').appendChild(modal_button_confirm);
		
		var modal_button_deny = document.createElement('button');
		modal_button_deny.id = 'wb-modal-window-button-deny';
		modal_button_deny.innerHTML = 'annulla';
		document.getElementById('wb-modal-window-footer').appendChild(modal_button_deny);
		/*
		var footer_structure ="<div><button id='wb-modal-window-button-deny' class='wb-button'>ANNULLA</button><button id='wb-modal-window-button-confirm' class='wb-button'>ELIMINA</button></div>";
		document.getElementById('wb-modal-window-footer').innerHTML = footer_structure;
		*/
		
	}
	
	open(){
		//
		if( this.formDataUrl != null){
			this.getData(this.formDataUrl);
			
		}
		this.setup();
		var bodyObj = document.getElementsByTagName("body")[0];
		bodyObj.style.overflow = 'hidden';
		var container = document.getElementById(this.modalId);
		container.style.display = 'block';
		
		if( this.onOpen != null ){
			this.onOpen();
		}
		//var btn = document.getElementById(this.buttonConfirmId);
		//monitorEvents(btn, "click");
		//document.getElementById('wb-modal-window-button-confirm').addEventListener( "click", function(){alert("Hello! I am an alert box!!");} );

	}
	
	close(){
		var bodyObj = document.getElementsByTagName("body")[0];
		bodyObj.style.overflow = 'auto';
		var container = document.getElementById(this.modalId);
		container.style.display = 'none';
	}
	
	confirm(){
		if( this.onConfirm != null ){
			this.onConfirm();
		}
	}
	
	getData( url ){
		var xhttp = new XMLHttpRequest();
		xhttp.onDataReadyAction = this.onDataReadyAction;
		xhttp.ObjReference = this;
		xhttp.onreadystatechange = function( ) {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById('jsonOutput').innerHTML = this.responseText;
				this.ObjReference.data = this.responseText;
				
				if( this.ObjReference.onDataReadyAction != null ){
					this.ObjReference.onDataReadyAction();
				}
				//this.ObjReference.fillFormJson( this.responseText );
			}
		};
		xhttp.open("GET", url , true);
		xhttp.send();
	}
	
	foreachData( fn ){
		var formDom = document.getElementById( this.formId );
		//var jsonData = JSON.parse(data);
		var jsonData = JSON.parse(this.data);
		var obj = jsonData;
		for (var key in obj){
			var attrName = key;
			var attrValue = obj[key];
			fn( attrName, attrValue );
			//var inputDom = this.getFormElementByName( 'FP_'+attrName );
			//if( inputDom != null){ inputDom.value=attrValue;}
		}
	}
	
	appendElement( el ){
		document.getElementById(this.formId).innerHTML += el;
	}
	
	appendChild( el ){
		document.getElementById(this.formId).appendChild( el );
	}
	
	onDataReadyAction(){
		this.fillFormJson( this.data );
	}
	
	fillFormJson( data ){
		
		var formDom = document.getElementById( this.formId );
		var jsonData = JSON.parse(data);

		var obj = jsonData[0];
		for (var key in obj){
			var attrName = key;
			var attrValue = obj[key];
			var inputDom = this.getFormElementByName( 'FP_'+attrName );
			if( inputDom != null){ inputDom.value=attrValue;}
		}
		
	}
	
	getFormElementByName( elName ){
		var formDom = document.getElementById( this.formId );
		var done = false;
		var el = null;
		var i = 0;
		while( !done ){
			if( formDom.elements[i].name == elName ){
				done = true;
				el = formDom.elements[i];
			}  
			if( i<formDom.elements.length-1 && !done ){
				i++;
			}else{
				done = true;
				
			}
		}
		return el;
	}
	
	
	attachEvents(){
		if( this.buttonConfirmId != null){
			var button_confirm = document.getElementById(this.buttonConfirmId);
			if( (button_confirm != null) & (this.onConfirm != undefined) ){
				button_confirm.addEventListener( "click", this.onConfirm );
			}
		}
		
		if( this.buttonDenyId != null){
			var button_deny = document.getElementById(this.buttonDenyId);
			if( (button_deny != null) & (this.onDeny != undefined) ){
				button_deny.addEventListener( "click", this.onDeny );
			}
		}
		
	}
	
}



function WB_Modal( id ){
	
	var self = this;

	var modalId = id;
	var modalDom = null;
	
	var data = null;
	
	this.title = null;
	var titleDom = null;
	
	var formDom = null;
	this.formAction = null;
	this.formMethod = 'POST';
	this.formDataUrl = null;
	
	var buttonConfirmDom = null;
	this.onConfirm = null;
	
	var buttonDenyDom = null;
	this.onDeny = null;
	
	this.onOpen = null;
	
	this.open = function(){
		
		if( self.formDataUrl != null){
			self.getData(self.formDataUrl);	
		}
		
		setup();
		var bodyDom = document.getElementsByTagName("body")[0];
		bodyDom.style.overflow = 'hidden';
		//var container = document.getElementById(this.modalId);
		modalDom.style.display = 'block';
		
		if( self.onOpen != null ){
			self.onOpen();
		}
		//var btn = document.getElementById(this.buttonConfirmId);
		//monitorEvents(btn, "click");
		//document.getElementById('wb-modal-window-button-confirm').addEventListener( "click", function(){alert("Hello! I am an alert box!!");} );

	}
	
	var setup = function ( ){
		
		if(modalId != null){
			modalDom = document.getElementById(modalId);
		}else{
			modalDom = document.getElementById('wb-modal-container');
			if( modalDom != null){
				modalDom.parentNode.removeChild(modalDom);
			}
			generate();
			generateSimpleModal();
			modalDom = document.getElementById('wb-modal-container');

			//this.modalId = 'wb-modal-container';
			//this.buttonConfirmId = 'wb-modal-window-button-confirm';
			//this.buttonDenyId = 'wb-modal-window-button-deny';
		}
		
		buttonConfirmDom = modalDom.getElementsByClassName('wb-modal-button-confirm')[0];
		buttonDenyDom = modalDom.getElementsByClassName('wb-modal-button-deny')[0];
		
		// Modifica titolo finestra
		titleDom = modalDom.getElementsByClassName('title')[0];
		if( titleDom != null){
			titleDom.innerHTML = self.title;
		}
		
		formDom = modalDom.getElementsByClassName('wb-modal-form')[0];
		if( formDom != null ){
			formDom.action = self.formAction;
			formDom.method = self.formMethod;
		}
		
		attachEvents();
	};
	
	
	/* Genera la modal window vuota */
	function generate() {
		var modal_container = document.createElement('div');
		modal_container.id = 'wb-modal-container';
		modal_container.style.display = 'none';
		document.body.appendChild(modal_container);
		
		var modal_background = document.createElement('div');
		modal_background.classList.add('wb-modal-background');
		modal_container.appendChild(modal_background);
		
		var modal_window = document.createElement('div');
		modal_window.id = 'wb-modal-window';
		modal_window.classList.add('wb-modal-window');
		modal_window.classList.add(this.windowStyle);
		modal_background.appendChild(modal_window);
		
		var modal_head = document.createElement('div');
		modal_head.id = 'wb-modal-window-head';
		modal_head.classList.add('head');
		modal_window.appendChild(modal_head);
		
		var modal_body = document.createElement('div');
		modal_body.id = 'wb-modal-window-body';
		modal_body.classList.add('body');
		modal_window.appendChild(modal_body);
		
		var modal_footer = document.createElement('div');
		modal_footer.id = 'wb-modal-window-footer';
		modal_footer.classList.add('footer');
		modal_window.appendChild(modal_footer);

	}
	
	
	function generateSimpleModal(){
		var head_structure = "<span class='title'>" + self.title + "</span>";
		document.getElementById('wb-modal-window-head').innerHTML = head_structure;
		
		var body_structure = "<p class='message'>" + self.message + "</p>"
		document.getElementById('wb-modal-window-body').innerHTML = body_structure;
		
		
		/*var modal_button_group_left = document.createElement('div');
		modal_button_confirm.id = 'wb-modal-window-button-confirm';
		*/
		var modal_button_confirm = document.createElement('button');
		modal_button_confirm.id = 'wb-modal-window-button-confirm';
		modal_button_confirm.innerHTML = 'elimina';
		document.getElementById('wb-modal-window-footer').appendChild(modal_button_confirm);
		
		var modal_button_deny = document.createElement('button');
		modal_button_deny.classList.add('wb-modal-button-deny');
		modal_button_deny.innerHTML = 'annulla';
		document.getElementById('wb-modal-window-footer').appendChild(modal_button_deny);
		
		//buttonConfirmDom = document.getElementById('wb-modal-window-button-confirm');
		//buttonDenyDom = document.getElementById('wb-modal-window-button-deny');

		/*
		var footer_structure ="<div><button id='wb-modal-window-button-deny' class='wb-button'>ANNULLA</button><button id='wb-modal-window-button-confirm' class='wb-button'>ELIMINA</button></div>";
		document.getElementById('wb-modal-window-footer').innerHTML = footer_structure;
		*/
		
	}
	
	
	this.getData=function( url ){
		var xhttp = new XMLHttpRequest();
		xhttp.onDataReadyAction = this.onDataReadyAction;
		xhttp.self = self;
		xhttp.onreadystatechange = function( ) {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById('jsonOutput').innerHTML = this.responseText;
				this.self.data = this.responseText;
				
				if( this.self.onDataReadyAction != null ){
					this.self.onDataReadyAction();
				}
				//this.ObjReference.fillFormJson( this.responseText );
			}
		};
		xhttp.open("GET", url , true);
		xhttp.send();
	}
	
	
	function foreachData( fn ){
		var formDom = document.getElementById( this.formId );
		//var jsonData = JSON.parse(data);
		var jsonData = JSON.parse(this.data);
		var obj = jsonData;
		for (var key in obj){
			var attrName = key;
			var attrValue = obj[key];
			fn( attrName, attrValue );
			//var inputDom = this.getFormElementByName( 'FP_'+attrName );
			//if( inputDom != null){ inputDom.value=attrValue;}
		}
	}
	/*
	appendElement( el ){
		document.getElementById(this.formId).innerHTML += el;
	}
	
	appendChild( el ){
		document.getElementById(this.formId).appendChild( el );
	}*/
	
	this.onDataReadyAction = function(){
		this.fillFormJson( this.data );
	}
	
	this.fillFormJson = function( data ){
		
		var formDom = document.getElementById( this.formId );
		var jsonData = JSON.parse(data);

		var obj = jsonData[0];
		for (var key in obj){
			var attrName = key;
			var attrValue = obj[key];
			var inputDom = this.getFormElementByName( 'FP_'+attrName );
			if( inputDom != null){ inputDom.value=attrValue;}
		}
		
	}
	
	function attachEvents(){
		if( buttonConfirmDom != null){
			if( self.onConfirm != undefined ){
				buttonConfirmDom.addEventListener( "click", self.onConfirm );
			}
		}
		
		if( buttonDenyDom != null){
			if( self.onDeny != undefined ){
				buttonDenyDom.addEventListener( "click", self.onDeny );
			}
		}
		
	}
	
	this.getFormElementByName = function( elName ){
		
		var done = false;
		var el = null;
		var i = 0;
		while( !done ){
			if( formDom.elements[i].name == elName ){
				done = true;
				el = formDom.elements[i];
			}  
			if( i<formDom.elements.length-1 && !done ){
				i++;
			}else{
				done = true;
				
			}
		}
		return el;
	}
	
	this.close=function(){
		var bodyObj = document.getElementsByTagName("body")[0];
		bodyObj.style.overflow = 'auto';
		modalDom.style.display = 'none';
	}
	
	this.onDeny = function(){
		self.close();
	}
	
	this.onConfirm = function(){
		
	}
	
	
	
}//fine classe

var WB_Modal2 ={
	modalId:null,
	modalDom:null,
	data:null,
	
	title:null,
	titleDom:null,
	
	open:function(){
		/*
		if( this.formDataUrl != null){
			this.getData(this.formDataUrl);
			
		}*/
		setup();
		var bodyDom = document.getElementsByTagName("body")[0];
		bodyDom.style.overflow = 'hidden';
		//var container = document.getElementById(this.modalId);
		modalDom.style.display = 'block';
		/*
		if( this.onOpen != null ){
			this.onOpen();
		}*/
		//var btn = document.getElementById(this.buttonConfirmId);
		//monitorEvents(btn, "click");
		//document.getElementById('wb-modal-window-button-confirm').addEventListener( "click", function(){alert("Hello! I am an alert box!!");} );

	},
	
	setup:function( ){
		
		if(modalId != null){
		   
		}else{
			this.generate();
			this.generateSimpleModal();
			this.modalId = 'wb-modal-container';
			this.buttonConfirmId = 'wb-modal-window-button-confirm';
			this.buttonDenyId = 'wb-modal-window-button-deny';
		}
		
		titleDom = modalDom.getElementsByClassName('title')[0];
		// Modifica titolo finestra
		if( titleDom != null){
			titleDom.innerHTML = title;
		}
		/*
		titleDom = modalDom.getElementsByClassName('title')[0];

		if( this.formId != null ){
			document.getElementById(this.formId).action = this.formAction;
		}
		
		this.attachEvents();*/
	},
	
};


/* ------------------------------------------------------------------------------------- 
 Autocomplete
*/

function WB_Autocomplete( cnf ){
	
	var inputDom = null;
	var inputId = null;
	var currentFocus = null;
	var url = null;
	var data = null;
	var formId = null;
	var formDom = null;
	var field = null;
	
	if( 'inputId' in cnf ){ inputId = cnf['inputId'];}
	if( 'inputDom' in cnf ){ 
		inputDom = cnf['inputDom'];
	}else{
		inputDom = document.getElementById(inputId);

	}

	if( "url" in cnf ){ url = cnf["url"]; }
	if( "onSelect" in cnf ){ selectElement = cnf["onSelect"]; }
	if( "field" in cnf ){ field = cnf["field"]; }

	if( "formId" in cnf ){ formDom = document.getElementById(cnf["formId"]); }

	var inputContainerDom = inputDom.parentNode;

	
	inputDom.addEventListener("input", function(){
		
		
		var xhttp = new XMLHttpRequest();
		xhttp.ObjReference = this;
		xhttp.onreadystatechange = function( ) {
			if (this.readyState == 4 && this.status == 200) {
				var jsonData = JSON.parse(this.responseText);
				data = JSON.parse(this.responseText)

				populate( jsonData );
				
			}
		};
		xhttp.open("GET", url , true);
		xhttp.send();

		
		
	});

	function populate( inputId ){
		/*close any already open lists of autocompleted values*/
      closeAllLists( );
	 
	  currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      inputContainerDom.appendChild(a);
	 
	  /*for each item in the array...*/
	  var a, b, i, val = this.value;
	  val = inputDom.value;
      for (i = 0; i < data.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (data[i]['ragione_sociale'].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + data[i]['ragione_sociale'].substr(0, val.length) + "</strong>";
          b.innerHTML += data[i]['ragione_sociale'].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + i + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          
		if( formDom != null){
			b.addEventListener("click", function(e){
				data_row = data[ this.getElementsByTagName("input")[0].value ];
				
				fillForm( e, this, inputDom, formDom, data_row);
			});
		}else{
			b.addEventListener("click", function(e ){
				data_row = data[ this.getElementsByTagName("input")[0].value ];

				selectElement( e, this, inputDom, data_row); 
		  	}				 
          );
		}	

          a.appendChild(b);
        }
      } 
	}
	
	
	
	function selectElement( e, div, input, d ){
		//var index = div.getElementsByTagName("input")[0].value;
		input.value = d[field];
		//div.getElementsByTagName("input")[0].value;

	}
	
	function fillForm( e, div, inputDom, formDom  ){
		var index = div.getElementsByTagName("input")[0].value;
		var dataRow = data[index];
		for (var key in dataRow){
			setFormElement( key, dataRow[key] );
		}
	}
	
	function setFormElement( elName, val){
		var done = false;
		var i = 0;
		var formEl = formDom.getElementsByTagName("input"); 
		while( !done ){
			if( formEl[i].name == elName ){
				done = true;
				formEl[i].value = val;
			}  
			if( i<formEl.length-1 && !done ){
				i++;
			}else{
				done = true;
			}
		}
	}
	
	
	function closeAllLists(elmnt) {
		/*close all autocomplete lists in the document,
		except the one passed as an argument:*/
		var x = document.getElementsByClassName("autocomplete-items");
		for (var i = 0; i < x.length; i++) {
		  if (elmnt != x[i] && elmnt != inputDom) {
			x[i].parentNode.removeChild(x[i]);
		  }
		}
	}
	
	
	/*execute a function when someone clicks in the document:*/
	document.addEventListener("click", function (e) {
    	closeAllLists(e.target);
	});
	
	
}

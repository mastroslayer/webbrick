
/*
* wb-modal-window-close-button: Pulsante chiudi finestra
*/
function wb_modal( title, msg, on_confirm, on_deny, on_close){
	/*var elem = document.createElement('div');
	elem.id = 'wb-modal-container';
	document.body.appendChild(elem);*/
	wb_generate_modal_window();
		
}


function wb_modal_attach_event(){
	var buttonClose = document.getElementById('wb-modal-window-button-close');
	if( buttonClose != null){
		buttonClose.addEventListener("click", function(){
    		var elem = document.getElementById('wb-modal-container');
	//var elem = document.getElementById('wb-modal-container').style.display = 'none';
	elem.style.display = 'none';
	//wb_modal_remove_event();
	document.body.removeChild(elem);
		});
	}	
}

function wb_modal_attach_event2( events ){
	
	var button_confirm = document.getElementById('wb-modal-window-button-confirm');
	if( (button_confirm != null) & (events.on_confirm != undefined) ){
		button_confirm.addEventListener( "click", events.on_confirm);
	}
	
	var button_close = document.getElementById('wb-modal-window-button-close');
	if( button_close != null){
		button_close.addEventListener("click", function(){
    		var elem = document.getElementById('wb-modal-container');
			elem.style.display = 'none';
			document.body.removeChild(elem);
		});
	}
	
}
/*
function wb_modal_remove_event(){
	var buttonClose = document.getElementById('wb-modal-window-button-close');
	if( buttonClose != null){
		document.removeEventListener("click", function(){
    			var elem = document.getElementById('wb-modal-container');
	//var elem = document.getElementById('wb-modal-container').style.display = 'none';
				elem.style.display = 'none';
	//wb_modal_remove_event();
	//document.body.removeChild(elem);
		});
	}
}
*/
function wb_modal_close(){
	var elem = document.getElementById('wb-modal-container');
	//var elem = document.getElementById('wb-modal-container').style.display = 'none';
	var bodyObj = document.getElementsByTagName("body")[0];
	bodyObj.style.overflow = 'auto';
	
	elem.style.display = 'none';
	document.body.removeChild(elem);
}

function wb_generate_modal_window( title, msg ){
	var elem = document.createElement('div');
	elem.id = 'wb-modal-container';
	document.body.appendChild(elem);
	
	var container = document.getElementById('wb-modal-container');
	var windows_el = "<div id='wb-modal-window-head' class='head'></div><div id='wb-modal-window-body' class='body'></div><div id='wb-modal-window-footer' class='footer'></div>"
	var element = "<div class='wb-modal-background'><div id='wb-modal-window'></div></div>"
	document.getElementById("wb-modal-container").innerHTML = element;
	document.getElementById("wb-modal-window").innerHTML = windows_el;
	
	var bodyObj = document.getElementsByTagName("body")[0];
	bodyObj.style.overflow = 'hidden';
	container.style.display = 'block';
}


function wb_modal_status( url ){
	wb_generate_modal_window();
	document.getElementById('wb-modal-window').innerHTML = "Waiting"; 
	if(!window.XMLHttpRequest){
		log_message("Your browser does not support the native XMLHttpRequest object.");
		return;
	}
    try{
            var xhr = new XMLHttpRequest();  
            xhr.previous_text = '';
             
            xhr.onload = function() { log_message("[XHR] Done. responseText: <i>" + xhr.responseText + "</i>"); };
            xhr.onerror = function() { log_message("[XHR] Fatal Error."); };
            xhr.onreadystatechange = function(){
			try{
                    if (this.readyState > 2){
                        log_message(result.message);
                        //update the progressbar
                        document.getElementById('wb-modal-window').innerHTML = this.responseText; 
                    }   
                }
                catch (e)
                {
                    log_message("<b>[XHR] Exception: " + e + "</b>");
                }
                 
                 
            };
     
            xhr.open("GET", url, true);
            //xhr.send("Making request...");      
        }
        catch (e){
            log_message("<b>[XHR] Exception: " + e + "</b>");
        }
    }
	
function fillWin( data ){
	     var window = document.getElementById("wb-modal-window");
		window.innerHTML = data;
}

function wb_modal_window( url ) {
	wb_generate_modal_window();	
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     //var window = document.getElementById("wb-modal-window");
		//window.innerHTML = this.responseText;
		fillWin( this.responseText );
	wb_modal_attach_event();	
	}
  };
  xhttp.open("GET", url, true);
  xhttp.send();
	
	
}	

function wb_modal_window3( url, events ) {
	wb_generate_modal_window();	
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		fillWin( this.responseText );
	wb_modal_attach_event2( events );	
	}
  };
  xhttp.open("GET", url, true);
  xhttp.send();
	
	
}	

function wb_modal_window2( url ) {
	wb_generate_modal_window();	
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState > 0) {
	document.getElementById("wb-modal-window").innerHTML = '';
     document.getElementById("wb-modal-window").innerHTML = this.responseText;
	this.responseText = null;	
    }
  };
  xhttp.open("GET", url, true);
  xhttp.send();
}	

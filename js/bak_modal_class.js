class wb_modal_prototype{

	constructor( config ) {
		this.generate_modal_window();
		this.events = {
			on_deny:config.on_deny,
			on_close:null,
			
		};
		this.wb_modal_attach_event(this.events);
		//this.on_deny = config.ondeny;
	}
	
	generate_modal_window(){
		var container = document.createElement('div');
		container.id = 'wb-modal-container';
		document.body.appendChild(container);

		var container = document.getElementById('wb-modal-container');
		var windows_el = "<div id='wb-modal-window-head' class='head'></div><div id='wb-modal-window-body' class='body'></div><div id='wb-modal-window-footer' class='footer'></div>"
		var element = "<div class='wb-modal-background'><div id='wb-modal-window'></div></div>"
		document.getElementById("wb-modal-container").innerHTML = element;
		document.getElementById("wb-modal-window").innerHTML = windows_el;

		var bodyObj = document.getElementsByTagName("body")[0];
		bodyObj.style.overflow = 'hidden';
		container.style.display = 'block';
	}
	
	
	wb_modal_attach_event( events ){
	
		var domObj = document.getElementById('wb-modal-window-button-confirm');
		var button_confirm = document.getElementById('wb-modal-window-button-confirm');
		if( (button_confirm != null) & (events.on_confirm != undefined) ){
			button_confirm.addEventListener( "click", events.on_confirm);
		}

		var button_deny = document.getElementById('wb-modal-window-button-deny');
		if( (button_deny != null) & (events.on_deny != undefined) ){
			button_deny.addEventListener( "click", events.on_deny);
		}		
		
		var button_close = document.getElementById('wb-modal-window-button-close');
		if( button_close != null){
			if( events.on_close != undefined ){
				button_close.addEventListener( "click", events.on_close);
				var elem = document.getElementById('wb-modal-container');
			}else{
				button_close.addEventListener("click", function(){wb_modal_close();});
			}
		}
	
	}
	
	close(){
		var body = document.getElementsByTagName("body")[0];
		body.style.overflow = 'auto';
		var container = document.getElementById('wb-modal-container');
		container.style.display = 'none';
		document.body.removeChild(container);	
	}
	
	get_close_function(){
		return this.close;
	}
	
	submit(){}
		
}


function wb_modal_confirm( pippo ){
	var config = { on_deny:pippo };
	var modal = new wb_modal_prototype( config );
	var footer_structure ="<div style='text-align:center;'><button id='wb-modal-window-button-deny' class='wb-button'>ANNULLA</button><button id='wb-modal-window-button-confirm' class='wb-button'>ELIMINA</button></div>";
	var body_structure = "<h3 style='text-align:center; font-size:28px; font-weight:900; padding-top:32px;'>Sei sicuro?</h3>";
	body_structure += "<p style='text-align:center; padding:16px; font-size:20px;' >Vuoi elimnare gli aricoli selezionati?</p>"
	//document.getElementById('wb-modal-window-head').innerHTML = 'Eliminare?';
	document.getElementById('wb-modal-window-body').innerHTML = body_structure;
	document.getElementById('wb-modal-window-footer').innerHTML = footer_structure;	
	modal.wb_modal_attach_event( config );

}






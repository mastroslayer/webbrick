/*
*
* Esempio definizione eventi
var add_button = {
	url:'fatture/vendita/new',
	on_close:function(){wb_modal_close();},
	on_confirm:function(){document.getElementById('form_modal').submit();},
	on_deny:function(){wb_modal_close();}
}

var my_modal = new wb_modal( );
my_modal.set_config( config_edit_cliente );	
my_modal.addEvent( 'click', 'myDomElement', function(){ modal_cliente.open(); } );	
*
*
*
*
* Elementi modal window
* -------------------------------------------------------
* wb-modal-window-button-close: Pulsante chiudi finestra
* wb-modal-window-button-confirm: Pulsante ok/salva
* wb-modal-window-button-deny: Pulsante annulla
* wb-modal-window-title: Titolo della finestra
* wb-modal-window-text
* wb-modal-window-form
*
* wb-modal-window-body
* wb-modal-window-footer
*
*/
	
	function wbModalWindowGenerate() {
		var container = document.createElement('div');
		container.id = 'wb-modal-container';
		container.style.display = 'none';
		document.body.appendChild(container);

		//var container = document.getElementById('wb-modal-container');
		var windows_el = "<div id='wb-modal-window-head' class='head'></div><div id='wb-modal-window-body' class='body'></div><div id='wb-modal-window-footer' class='footer'></div>"
		var element = "<div class='wb-modal-background'><div id='wb-modal-window'></div></div>"
		document.getElementById("wb-modal-container").innerHTML = element;
		document.getElementById("wb-modal-window").innerHTML = windows_el;

		//var bodyObj = document.getElementsByTagName("body")[0];

	}
	
	
	function wbModalWindowAttachEvents( events ){
	
		var domObj = document.getElementById('wb-modal-window-button-confirm');
		var button_confirm = document.getElementById('wb-modal-window-button-confirm');
		if( (button_confirm != null) & (events.on_confirm != undefined) ){
			button_confirm.addEventListener( "click", events.on_confirm);
		}

		var button_deny = document.getElementById('wb-modal-window-button-deny');
		if( (button_deny != null) & (events.on_deny != undefined) ){
			button_deny.addEventListener( "click", events.on_deny);
		}		
		
		var button_close = document.getElementById('wb-modal-window-button-close');
		if( button_close != null){
			if( events.on_close != undefined ){
				button_close.addEventListener( "click", events.on_close);
				var elem = document.getElementById('wb-modal-container');
			}else{
				button_close.addEventListener("click", function(){wbModalClose();});
			}
		}
	
	}
	
	/*
	* Chiudi la finestra di dialogo
	*/
	function wbModalClose(){
		var body = document.getElementsByTagName("body")[0];
		body.style.overflow = 'auto';
		var container = document.getElementById('wb-modal-container');
		container.style.display = 'none';
		document.body.removeChild(container);	
	}

	function wbModalOpen(){
		var bodyObj = document.getElementsByTagName("body")[0];
		bodyObj.style.overflow = 'hidden';
		var container = document.getElementById('wb-modal-container');
		container.style.display = 'block';
	}




	function wbModalLoadUrl( url, events ){

		var xhttp = new XMLHttpRequest();
  		xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
     	var window = document.getElementById("wb-modal-window");
		window.innerHTML = this.responseText;
		wbModalWindowAttachEvents( events );
		
		}
	  };
	  xhttp.open("GET", url, true);
	  xhttp.send();
		
	}



	function wbModalForm( url, config ){
		var events = {
			on_close:function(){wbModalClose();},
			on_confirm:function(){document.getElementById('wb-modal-window-form').submit();}, 
			on_deny:function(){wbModalClose();}
		};
		wbModalWindowGenerate();
		wbModalLoadUrl( url, events );
		
		//wbModalWindowAttachEvents( events );
		wbModalOpen();
	}



	function wbModalConfirm( confirm_action, deny_action, config){
		var events = { on_confirm:confirm_action, on_deny:deny_action };
		wbModalWindowGenerate();
		var footer_structure ="<div style='text-align:center;'><button id='wb-modal-window-button-deny' class='wb-button'>ANNULLA</button><button id='wb-modal-window-button-confirm' class='wb-button'>ELIMINA</button></div>";
		var body_structure = "<h3 style='text-align:center; font-size:28px; font-weight:900; padding-top:32px;'>Sei sicuro?</h3>";
		body_structure += "<p style='text-align:center; padding:16px; font-size:20px;' >Vuoi elimnare gli aricoli selezionati?</p>"
		//document.getElementById('wb-modal-window-head').innerHTML = 'Eliminare?';
		document.getElementById('wb-modal-window-body').innerHTML = body_structure;
		document.getElementById('wb-modal-window-footer').innerHTML = footer_structure;
		wbModalWindowAttachEvents( events );
		wbModalOpen();
	}







/*
* Gestione tabelle
* Esempio definizione eventi
*
*/

/*
*
*/
	function wbTableDisplayColumn( tableId, column, state ){
		
		var tbl  = document.getElementById(tableId);
		var rows = tbl.getElementsByTagName('tr');

		var cels = rows[0].getElementsByTagName('th');
		cels[column].style.display=state;
		for (var row=1; row<rows.length;row++) {
		  var cels = rows[row].getElementsByTagName('td');
		  cels[column].style.display=state;
		}
		
	}





class wb_modal {


	constructor() {
		this.url = null;
		this.show_window_after_ajax = false;
		this.on_ajax_submit = null;
	}


	close(){
		var body = document.getElementsByTagName("body")[0];
		body.style.overflow = 'auto';
		var container = document.getElementById('wb-modal-container');
		container.style.display = 'none';
		document.body.removeChild(container);	
	}
	/*
	this.setup(){

	}
	*/
	create(){
		var container = document.createElement('div');
		container.id = 'wb-modal-container';
		container.style.display = 'none';
		document.body.appendChild(container);

		//var container = document.getElementById('wb-modal-container');
		var windows_el = "<div id='wb-modal-window-head' class='head'></div><div id='wb-modal-window-body' class='body'></div><div id='wb-modal-window-footer' class='footer'></div>"
		var element = "<div class='wb-modal-background'><div id='wb-modal-window'></div></div>"
		document.getElementById("wb-modal-container").innerHTML = element;
		document.getElementById("wb-modal-window").innerHTML = windows_el;
	}

	show(){
		var bodyObj = document.getElementsByTagName("body")[0];
		bodyObj.style.overflow = 'hidden';
		var container = document.getElementById('wb-modal-container');
		container.style.display = 'block';
	}

	open(){
		this.create();
		if( this.url != null ){
			this.ajax( this.url );
			var form = document.getElementById('wb-modal-window-form');
			//if( wb-modal-window-form)
		}
		//this.setup();
		this.show();

	}
	
	openHttp( url ){
		this.create();
		this.show_window_after_ajax = true;
		this.ajax( url );
		//this.show();
		

	}

	setup(){
		var ButtonClose = document.getElementById('wb-modal-window-button-close');
		var ButtonConfirm = document.getElementById('wb-modal-window-button-confirm');
		var Form = document.getElementById('wb-modal-window-form');
		
		if( ButtonClose != undefined ){
			ButtonClose.addEventListener( 'click', this.close );
		}
		
		if( ButtonConfirm != undefined ){
			if( Form != undefined ){
				if( this.on_confirm != undefined ){
					var tmp = this.on_confirm;
					ButtonConfirm.addEventListener( 'click', function(){ tmp(); } );

				}
				else if( this.on_ajax_submit == null){
					
					ButtonConfirm.addEventListener( 'click', function(){ Form.submit(); } );
					
				}else{
					var azz = this.sendAjaxForm;
					ButtonConfirm.addEventListener( 'click', function(){ azz(); } );
				}
			}
		}
		if( document.getElementById( 'wb-modal-window') != null ){
			document.onkeydown = function(evt) {
				evt = evt || window.event;
				if (evt.keyCode == 27) {
					var body = document.getElementsByTagName("body")[0];
					body.style.overflow = 'auto';
					var container = document.getElementById('wb-modal-container');
					container.style.display = 'none';
					document.body.removeChild(container);
					document.onkeydown = null;
					//alert('Esc key pressed.');
				}
			};
		}
	}

	addEvent ( event, domObj, fn ){
		document.getElementById(domObj).addEventListener( event, fn);
	}
	
	
	
	
	
	sendAjaxForm(){
		var form = document.getElementById('wb-modal-window-form');
		//form.action 
		var data = new FormData( form );
		var xhttp = new XMLHttpRequest();
		var return_data = null;
		//xhttp.ClassRef = this.selfservice;
		xhttp.ClassRef = wb_modal;
		//xhttp.show_window_after_ajax = this.show_window_after_ajax;
		//xhttp.DomElm = dom;
		xhttp.return_data = return_data;
		xhttp.on_ajax_submit = this.on_ajax_submit;
		var kkkkk = 'kjhk';
		xhttp.onreadystatechange = function( ) {
			if (this.readyState == 4 && this.status == 200) {
				this.on_ajax_submit( this.responseText );
				//self.on_ajax_submit();
				//this.ClassRef.on_ajax_submit();
				//var window = document.getElementById( 'wb-modal-window' );
				//window.innerHTML = this.responseText;
				//this.ClassRef.setup();
				//xhttp.return_data = this.responseText;
				//if( this.show_window_after_ajax ){ this.ClassRef.show(); }
				//var pippo = document.getElementById('wb-modal-window-form');
				//var data = new FormData( pippo );
				var jhgf = data;
				//wbModalWindowAttachEvents( events );
			}
		};
		xhttp.open("GET", form.action , true);
		xhttp.send();
	}
	
	
	
	
	ajax( url ){
		//this.create();
		//var ha = this;
		var xhttp = new XMLHttpRequest();
		xhttp.ClassRef = this;
		xhttp.show_window_after_ajax = this.show_window_after_ajax;
		//xhttp.DomElm = dom;
		xhttp.onreadystatechange = function( ) {
			if (this.readyState == 4 && this.status == 200) {
				var window = document.getElementById( 'wb-modal-window' );
				window.innerHTML = this.responseText;
				this.ClassRef.setup();
				if( this.show_window_after_ajax ){ this.ClassRef.show(); }
				//var pippo = document.getElementById('wb-modal-window-form');
				//var data = new FormData( pippo );
				//var jhgf = data;
				//wbModalWindowAttachEvents( events );
			}
		};
		xhttp.open("GET", url, true);
		xhttp.send();

	}

	set_config( conf ){
		this.url = conf.url;
		this.on_ajax_submit = conf.on_ajax_submit;
		this.on_confirm = conf.on_confirm;
		this.setup();
	}


}





/* 
*
* Esempio definizione eventi
var add_button = {
	url:'fatture/vendita/new',
	on_close:function(){wb_modal_close();},
	on_confirm:function(){document.getElementById('form_modal').submit();},
	on_deny:function(){wb_modal_close();}
}

var my_modal = new wb_modal( );
my_modal.set_config( config_edit_cliente );	
my_modal.addEvent( 'click', 'myDomElement', function(){ modal_cliente.open(); } );	
*
*
*
*
* Elementi modal window
* -------------------------------------------------------
* wb-modal-window-button-close: Pulsante chiudi finestra
* wb-modal-window-button-confirm: Pulsante ok/salva
* wb-modal-window-button-deny: Pulsante annulla
* wb-modal-window-title
* wb-modal-window-text
* wb-modal-window-form
*
*/

var exdata = {
	"status": "success",
	"data": [
		{
		"produttore": "Molecule Man",
		"codice_produttore": "codice produttore",
		"ean": "00000000000000",
		"descrizione": "Descrizione prodotto",
		"prezzo":100,
		"quantita": 10,
		"prezzo": 100,
		"raee": 0,
		"siae": 0,
		"iva": 22	
    	},
    	{
		"produttore": "Molecule Man",
		"codice_produttore": "codice produttore",
		"ean": "00000000000000",
		"descrizione": "Descrizione prodotto",
		"prezzo":100,
		"quantita": 10,
		"prezzo": 100,
		"raee": 0,
		"siae": 0,
		"iva": 22	
		}
  ]
}


class WBTable {


	constructor( ) { 
		//this.name = name; 
		this.url = null;
		this.show_window_after_ajax=false;
		this.tableId='table_name';
		this.formActionId = null;
		this.formActionFieldName = 'id';
		this.formInputIdPrefix = 'prefix';
		
		this.selectedItemsCount = 0 ;
	}
	
	onSelection( checkboxId ){
		var formAction = document.getElementById(this.formActionId);
		var checkbox = document.getElementById( checkboxId );
		
		if( checkbox.checked == true ){
			var inputField = document.createElement("INPUT");
			inputField.setAttribute("type", "text");
			inputField.setAttribute("name", this.formActionFieldName);
			inputField.setAttribute("value", checkbox.value);
			inputField.setAttribute("id", this.formInputIdPrefix+checkbox.value);
			formAction.appendChild(inputField);
			this.selectedItemsCount ++;
		}else{
			inputField = document.getElementById(this.formInputIdPrefix+checkbox.value);
			if( inputField != null){
				formAction.removeChild(inputField);
				this.selectedItemsCount --;
			}
		}
	}
	
	addRow( data ){
		var html_row = '<tr>';
		
		html_row += '<td>'+data.produttore+'</td>';
		
		html_row += '</tr>';
		//var html_row = '<tr><td>AAAA</td></tr>';
		var table = document.getElementById(this.tableId);
		//var table_body = table.getElementsByTagName('tbody');
		table.innerHTML += html_row ;
		
	}
	
	removeRow( rowId ){
		
	}
	
	updateRow(){
		
	}
	
	setData(){
		
	}
	
	readJSON( jsondata ){
		var data = jsondata['data'];
		for( var i=0; data.length; i++ ){
			this.addRow( data[i] );
			
			
			
		}
		
	}
	
	
	
}





class WBModalForm{
	
	constructor(){
		this.modalId=null;
		
		this.data = null;
		this.formDataUrl = null;
		
		this.formId = null;
		this.formAction = null;
		
		this.ObjReference = this;
		
		this.title = 'Title';
		this.titleId = null;
		
		this.message = 'Message';
	}
	
	
	
	
	
	
	setup( ){
		
		if(this.modalId != null){
		   
		}else{
			this.generate();
			this.generateSimpleModal();
		}
		
		
		if( this.formTitleId != null){
			document.getElementById(this.formTitleId).innerHTML = this.formTitle;
		}
		document.getElementById(this.formId).action = this.formAction;
	}
	
	
	/* Genera la modal window vuota */
	generate() {
		var container = document.createElement('div');
		container.id = 'wb-modal-container';
		container.style.display = 'none';
		document.body.appendChild(container);

		//var container = document.getElementById('wb-modal-container');
		var windows_el = "<div id='wb-modal-window-head' class='head'></div><div id='wb-modal-window-body' class='body'></div><div id='wb-modal-window-footer' class='footer'></div>"
		var element = "<div class='wb-modal-background'><div id='wb-modal-window'></div></div>"
		document.getElementById("wb-modal-container").innerHTML = element;
		document.getElementById("wb-modal-window").innerHTML = windows_el;

		//var bodyObj = document.getElementsByTagName("body")[0];

	}
	
	generateSimpleModal(){
		var footer_structure ="<div style='text-align:center;'><button id='wb-modal-window-button-deny' class='wb-button'>ANNULLA</button><button id='wb-modal-window-button-confirm' class='wb-button'>ELIMINA</button></div>";
		var body_structure = "<h3 style='text-align:center; font-size:28px; font-weight:900; padding-top:32px;'>";
		body_structure += this.title + "</h3>";
		body_structure += "<p style='text-align:center; padding:16px; font-size:20px;' >";
		body_structure += this.message+"</p>"
		document.getElementById('wb-modal-window-body').innerHTML = body_structure;
		document.getElementById('wb-modal-window-footer').innerHTML = footer_structure;
		
	}
	
	open(){
		//
		if( this.formDataUrl != null){
			this.getJsonData(this.formDataUrl);
			
		}
		this.setup();
		var bodyObj = document.getElementsByTagName("body")[0];
		bodyObj.style.overflow = 'hidden';
		var container = document.getElementById(this.modalId);
		container.style.display = 'block';
		
		if( this.onOpen != null ){
			this.onOpen();
		}
	}
	
	close(){
		var bodyObj = document.getElementsByTagName("body")[0];
		bodyObj.style.overflow = 'auto';
		var container = document.getElementById(this.modalId);
		container.style.display = 'none';
	}
	
	confirm(){
		if( this.onConfirm != null ){
			this.onConfirm();
		}
	}
	
	getJsonData( url ){
		var xhttp = new XMLHttpRequest();
		xhttp.onDataReadyAction = this.onDataReady;
		xhttp.ObjReference = this;
		xhttp.onreadystatechange = function( ) {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById('jsonOutput').innerHTML = this.responseText;
				this.ObjReference.data = this.responseText;
				
				if( this.onDataReadyAction != null ){
					this.onDataReadyAction();
				}
				this.ObjReference.fillFormJson( this.responseText );
			}
		};
		xhttp.open("GET", url , true);
		xhttp.send();
	}
	
	fillFormJson( data ){
		
		var formDom = document.getElementById( this.formId );
		var jsonData = JSON.parse(data);

		var obj = jsonData[0];
		for (var key in obj){
			var attrName = key;
			var attrValue = obj[key];
			var inputDom = this.getFormElementByName( 'FP_'+attrName );
			if( inputDom != null){ inputDom.value=attrValue;}
		}
		
	}
	
	getFormElementByName( elName ){
		var formDom = document.getElementById( this.formId );
		var done = false;
		var el = null;
		var i = 0;
		while( !done ){
			if( formDom.elements[i].name == elName ){
				done = true;
				el = formDom.elements[i];
			}  
			if( i<formDom.elements.length-1 && !done ){
				i++;
			}else{
				done = true;
				
			}
		}
		return el;
	}
	
}





class WBModalConfirm{
	
	constructor( id ){
		this.title = 'Title';
		this.message = 'Message';
		this.onClose = null;
		this.onDeny = null;
		this.onConfirm = null;
	}
	
	generate() {
		var container = document.createElement('div');
		container.id = 'wb-modal-container';
		container.style.display = 'none';
		document.body.appendChild(container);

		//var container = document.getElementById('wb-modal-container');
		var windows_el = "<div id='wb-modal-window-head' class='head'></div><div id='wb-modal-window-body' class='body'></div><div id='wb-modal-window-footer' class='footer'></div>"
		var element = "<div class='wb-modal-background'><div id='wb-modal-window'></div></div>"
		document.getElementById("wb-modal-container").innerHTML = element;
		document.getElementById("wb-modal-window").innerHTML = windows_el;

		//var bodyObj = document.getElementsByTagName("body")[0];

	}
	
	setup(){
		
		this.generate();
		var footer_structure ="<div style='text-align:center;'><button id='wb-modal-window-button-deny' class='wb-button'>ANNULLA</button><button id='wb-modal-window-button-confirm' class='wb-button'>ELIMINA</button></div>";
		var body_structure = "<h3 style='text-align:center; font-size:28px; font-weight:900; padding-top:32px;'>";
		body_structure += this.title + "</h3>";
		body_structure += "<p style='text-align:center; padding:16px; font-size:20px;' >";
		body_structure += this.message+"</p>"
		document.getElementById('wb-modal-window-body').innerHTML = body_structure;
		document.getElementById('wb-modal-window-footer').innerHTML = footer_structure;
		//wbModalWindowAttachEvents( events );
		//wbModalOpen();
	}
	
	close(){
		var body = document.getElementsByTagName("body")[0];
		body.style.overflow = 'auto';
		var container = document.getElementById('wb-modal-container');
		container.style.display = 'none';
		document.body.removeChild(container);	
	}

	open(){
		this.setup();
		var bodyObj = document.getElementsByTagName("body")[0];
		bodyObj.style.overflow = 'hidden';
		var container = document.getElementById('wb-modal-container');
		container.style.display = 'block';
	}
	
}
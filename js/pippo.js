/*
*
* Esempio definizione eventi
var add_button = {
	url:'fatture/vendita/new',
	on_close:function(){wb_modal_close();},
	on_confirm:function(){document.getElementById('form_modal').submit();},
	on_deny:function(){wb_modal_close();}
}
*
*
*
*
* Elementi modal window
* -------------------------------------------------------
* wb-modal-window-button-close: Pulsante chiudi finestra
* wb-modal-window-button-confirm: Pulsante ok/salva
* wb-modal-window-button-deny: Pulsante annulla
* wb-modal-window-title
* wb-modal-window-text
* wb-modal-window-form
*
*/
	
	function wbModalWindowGenerate(){
		var container = document.createElement('div');
		container.id = 'wb-modal-container';
		container.style.display = 'none';
		document.body.appendChild(container);

		//var container = document.getElementById('wb-modal-container');
		var windows_el = "<div id='wb-modal-window-head' class='head'></div><div id='wb-modal-window-body' class='body'></div><div id='wb-modal-window-footer' class='footer'></div>"
		var element = "<div class='wb-modal-background'><div id='wb-modal-window'></div></div>"
		document.getElementById("wb-modal-container").innerHTML = element;
		document.getElementById("wb-modal-window").innerHTML = windows_el;

		//var bodyObj = document.getElementsByTagName("body")[0];

	}
	
	
	function wbModalWindowAttachEvents( events ){
	
		var domObj = document.getElementById('wb-modal-window-button-confirm');
		var button_confirm = document.getElementById('wb-modal-window-button-confirm');
		if( (button_confirm != null) & (events.on_confirm != undefined) ){
			button_confirm.addEventListener( "click", events.on_confirm);
		}

		var button_deny = document.getElementById('wb-modal-window-button-deny');
		if( (button_deny != null) & (events.on_deny != undefined) ){
			button_deny.addEventListener( "click", events.on_deny);
		}		
		
		var button_close = document.getElementById('wb-modal-window-button-close');
		if( button_close != null){
			if( events.on_close != undefined ){
				button_close.addEventListener( "click", events.on_close);
				var elem = document.getElementById('wb-modal-container');
			}else{
				button_close.addEventListener("click", function(){wbModalClose();});
			}
		}
	
	}
	
	/*
	* Chiudi la finestra di dialogo
	*/
	function wbModalClose(){
		var body = document.getElementsByTagName("body")[0];
		body.style.overflow = 'auto';
		var container = document.getElementById('wb-modal-container');
		container.style.display = 'none';
		document.body.removeChild(container);	
	}

	function wbModalOpen(){
		var bodyObj = document.getElementsByTagName("body")[0];
		bodyObj.style.overflow = 'hidden';
		var container = document.getElementById('wb-modal-container');
		container.style.display = 'block';
	}




	function wbModalLoadUrl( url, events ){

		var xhttp = new XMLHttpRequest();
  		xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
     	var window = document.getElementById("wb-modal-window");
		window.innerHTML = this.responseText;
		wbModalWindowAttachEvents( events );
		
		}
	  };
	  xhttp.open("GET", url, true);
	  xhttp.send();
		
	}



	function wbModalForm( url, config ){
		var events = {
			on_close:function(){wbModalClose();},
			on_confirm:function(){document.getElementById('wb-modal-window-form').submit();}, 
			on_deny:function(){wbModalClose();}
		};
		wbModalWindowGenerate();
		wbModalLoadUrl( url, events );
		
		//wbModalWindowAttachEvents( events );
		wbModalOpen();
	}



	function wbModalConfirm( confirm_action, deny_action, config){
		var events = { on_confirm:confirm_action, on_deny:deny_action };
		wbModalWindowGenerate();
		var footer_structure ="<div style='text-align:center;'><button id='wb-modal-window-button-deny' class='wb-button'>ANNULLA</button><button id='wb-modal-window-button-confirm' class='wb-button'>ELIMINA</button></div>";
		var body_structure = "<h3 style='text-align:center; font-size:28px; font-weight:900; padding-top:32px;'>Sei sicuro?</h3>";
		body_structure += "<p style='text-align:center; padding:16px; font-size:20px;' >Vuoi elimnare gli aricoli selezionati?</p>"
		//document.getElementById('wb-modal-window-head').innerHTML = 'Eliminare?';
		document.getElementById('wb-modal-window-body').innerHTML = body_structure;
		document.getElementById('wb-modal-window-footer').innerHTML = footer_structure;
		wbModalWindowAttachEvents( events );
		wbModalOpen();
	}







/*
* Gestione tabelle
* Esempio definizione eventi
*
*/

/*
*
*/
	function wbTableDisplayColumn( tableId, column, state ){
		
		var tbl  = document.getElementById(tableId);
		var rows = tbl.getElementsByTagName('tr');

		var cels = rows[0].getElementsByTagName('th');
		cels[column].style.display=state;
		for (var row=1; row<rows.length;row++) {
		  var cels = rows[row].getElementsByTagName('td');
		  cels[column].style.display=state;
		}
		
	}





	class wb_modal {
		
		
		constructor( ) { 
			this.name = name; 
			this.url = null;
		}

		
		close(){
			var body = document.getElementsByTagName("body")[0];
			body.style.overflow = 'auto';
			var container = document.getElementById('wb-modal-container');
			container.style.display = 'none';
			document.body.removeChild(container);	
		}
		/*
		this.setup(){
			
		}
		*/
		create(){
			var container = document.createElement('div');
			container.id = 'wb-modal-container';
			container.style.display = 'none';
			document.body.appendChild(container);

			//var container = document.getElementById('wb-modal-container');
			var windows_el = "<div id='wb-modal-window-head' class='head'></div><div id='wb-modal-window-body' class='body'></div><div id='wb-modal-window-footer' class='footer'></div>"
			var element = "<div class='wb-modal-background'><div id='wb-modal-window'></div></div>"
			document.getElementById("wb-modal-container").innerHTML = element;
			document.getElementById("wb-modal-window").innerHTML = windows_el;
		}
		
		show(){
			var bodyObj = document.getElementsByTagName("body")[0];
			bodyObj.style.overflow = 'hidden';
			var container = document.getElementById('wb-modal-container');
			container.style.display = 'block';
		}
		
		open(){
			this.create();
			if( this.url != null ){
				this.ajax( this.url );
				var form = document.getElementById('wb-modal-window-form');
				//if( wb-modal-window-form)
			}
			//this.setup();
			this.show();
			
		}
		
		setup(){
			var ButtonClose = document.getElementById('wb-modal-window-button-close');
			var ButtonConfirm = document.getElementById('wb-modal-window-button-confirm');
			var Form = document.getElementById('wb-modal-window-form');
			
			
			if( ButtonClose != undefined ){
				ButtonClose.addEventListener( 'click', this.close );
			}
			
			if( Form != undefined ){
				
				if( ButtonConfirm != undefined ){
					ButtonConfirm.addEventListener( 'click', function(){ Form.submit(); } );
				}
				
				
			}
			
			
			
			
		}
		
		addEvent ( event, domObj, fn ){
			document.getElementById(domObj).addEventListener( event, fn);
		}
		
		
		ajax( url ){
			//var ha = this;
			var xhttp = new XMLHttpRequest();
			xhttp.ClassRef = this;
			xhttp.onreadystatechange = function( ) {
				if (this.readyState == 4 && this.status == 200) {
					var window = document.getElementById("wb-modal-window");
					window.innerHTML = this.responseText;
					this.ClassRef.setup();
					//wbModalWindowAttachEvents( events );
				}
			};
			xhttp.open("GET", url, true);
			xhttp.send();
			
		}
		
		set_config( conf ){
			this.url = conf.url;
			
		}
		
		
	}




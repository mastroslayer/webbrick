<?php $this->startBlock('wb-xray'); ?>

<?php 

	$logger = $GLOBALS['WB_LOGGER'];
	$logs = $logger->getLogs();


?>

<style>
	#wb-xray{
		width: 100%;
    	height: 200px;
    	background-color: white;
    	z-index: 30;
    	position: absolute;
    	bottom: 200px;	
		border-top: 1px solid black;
	}
	
	.wb-xray-header{
		height: 50px;	
	}
	
	.wb-xray-header > .wb-xray-button{
		display: inline-block;
		float: right;
		
	}
	
	.wb-xray-message{
		border-top:1px solid grey;
		
	}
	
	#wb-xray-title{
		
		display: inline-block;
		vertical-align: middle;
		font-size: 20px;
		padding: 8px;
		font-family: sans-serif;
		color: black;
	}
	.wb-xray-body{
		background-color: white;
	}
	
	.wb-xray-log-table{
		width: 100%;
		
	}
	
	.wb-xray-log-table > tbody{
		    height: 150px;
    	display: block;
    	overflow-y: scroll;	
	}
	
	.wb-xray-log-table > tbody > tr{
		
		
	}
	
	.wb-xray-log-table > tbody > th, td{
  		text-align: left;
  		padding: 4px;
		
	}
	.wb-xray-log-table > tbody > tr:nth-child(even) {background-color: #f2f2f2;}
	
	
	.wb-xray-log-table > tbody > tr:hover {background-color: lightgreen;}
	.wb-xray-log-table > tbody > tr > .log-message-col {
		width:100%;
		
	}
	
	.wb-xray-tab-list{
		//height:30px;
		   display: block;
    	overflow: auto;
	}
	.wb-xray-tab-list > div{
		display: inline-block;
		float: left;
		cursor: pointer;
		border: 1px solid grey;
		border-bottom: none;
		padding: 8px;
	}
	
	.wb-xray-tab{
		display:none;
	}
	
	
	.wb-var-tree{
		list-style-type: none;
	}
	
	.wb-var-tree > li {
		display: none;
	}
	.wb-var-tree > li > span{
		font-weight: 800;
		color: black;
	}
	
	.wb-var-tree-icon-folded{
		background-image: url(imgs/icons/google-md/add-black-18dp.svg);
		display: inline-block;
		width: 18px;
		height: 18px;
	}

	
	.wb-var-tree-icon-unfolded{
		background-image: url(imgs/icons/google-md/remove-black-18dp.svg);
		display: inline-block;
		width: 18px;
		height: 18px;
	}
	
	
	.wb-var-list-folded li{
		display: none;
		
	}
	
	.wb-var-list-unfolded li{
		display: list-item;
		
	}	
	
	.wb-var-list-folded ~ li > div{
		background-image: url(imgs/icons/google-md/add-black-18dp.svg);
		display: inline-block;
		width: 18px;
		height: 18px;
		
	}
	
	.wb-var-list-unfolded ~ li > div{
		background-image: url(imgs/icons/google-md/remove-black-18dp.svg);
		display: inline-block;
		width: 18px;
		height: 18px;	
	}
</style>

<script>
	function xrayClose(){
		document.getElementById('wb-xray').style.display= 'none';	
	}

	function xrayShowTab( tabname, title ){
		//logtab = document.getElementById('wb-xray-log-tab');
		//querytab = document.getElementById('wb-xray-query-tab');
		
		var titleDom = document.getElementById('wb-xray-title');
		var tablist = ["wb-xray-log-tab", "wb-xray-query-tab", "wb-xray-variables-tab"]; 
		
		titleDom.innerHTML = title;	
		tablist.forEach(function (item, index, array) {
			var tab = document.getElementById(item);
			if(tabname == item){
				tab.style.display= 'block';
			}else{
				tab.style.display= 'none';
				
			}
		});		
	}
	
	
	function expand( item, index, array){
		if(	item.style.display == 'block'){
			item.style.display = 'none';
		}else{
			item.style.display = 'block';
		}
	}
	
	function expandVar(domID){
		ulDom = document.getElementById(domID);
		var parent = ulDom.parentElement;
		var childList = ulDom.children;
		var img = parent.getElementsByTagName('div')[0];
		
		if(	img.className == 'wb-var-tree-icon-folded'){
			img.className = 'wb-var-tree-icon-unfolded';
			//ulDom.className = 'wb-var-list-unfolded';
			//ulDom.style = "background-image: url(imgs/icons/google-md/remove-black-18dp.svg)";
	
		}else{
			img.className = 'wb-var-tree-icon-folded'

			//ulDom.className = 'wb-var-list-folded';
			//ulDom.getElementsByClassName;
			//ulDom.style = "background-image: url(imgs/icons/google-md/add-black-18dp.svg)";

		}
		//var childList = ulDom.parentNode.getElementsByTagName('li');
		for (var i = 0, len = childList.length; i < len; i++) {
			expand(childList[i]);
		}

		//childList.forEach(expand);
		
	}
	

	
</script>


<?php 
	function printVariables( $invar, $startIndex=0 ){
		$out = null;
		$index = $startIndex+1;
		foreach($invar as $key=>$var){
			if( is_array($var) ){
				if($key != 'GLOBALS'){
					
					//echo 'Vediqui';
					echo '<li>';
					echo '<div class="wb-var-tree-icon-folded"></div>';
					echo '<span onclick="expandVar('.'\'wb-var-list-'.$index.'\');">'.$key.'</span>'; 

					echo '<ul class="wb-var-tree" id="wb-var-list-'.$index.'">';
					$index++;

					printVariables($var, $index);

					echo '</ul></li>';
				}

			}elseif(is_object($var)){
				if($key != 'GLOBALS'){

					//echo 'Vediqui';
					echo '<li>';

					echo '<div class="wb-var-tree-icon-folded"></div>';
					echo '<span onclick="expandVar('.'\'wb-var-list-'.$index.'\');">Object: '.$key.'</span>'; 
					echo '<ul class="wb-var-tree" id="wb-var-list-'.$index.'">';

					//echo '<ul>';
					//printVariables(object_to_array($var));
					$index++;

					printVariables( (array)$var, $index );
					echo '</ul></li>';
					//echo '</ul>';
				}
			}
			
			else{
				/*if(is_object($var) ){
					
					echo '<li>Object: '.$key.'<pre>';
					//print_r($var);
					//$tmp = get_object_vars($var);
					//print_r($tmp);
					//printVariables((array)get_object_vars($var));
					var_dump($var);
					echo '</pre></li>';
				}else{*/
					echo '<li>'.$key.':'.$var.'</li>';
				//}
			}
		}
		
		//return $out;
	}

/*function object_to_array($data)
{
    if (is_array($data) || is_object($data))
    {
        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = object_to_array($value);
        }
        return $result;
    }
    return $data;
}*/

?>
<div id="wb-xray">
	<div class="wb-xray-header">
		<h6 id="wb-xray-title">LOGS</h6>
		<div class="wb-xray-button" onclick="xrayClose();">close</div>
	</div>
	<div class="wb-xray-body">
		
		<div class="wb-xray-tab-list">
			<div onclick="xrayShowTab('wb-xray-log-tab', 'Logs');">Logs</div>
			<div onclick="xrayShowTab('wb-xray-query-tab', 'Query');">Query</div>
			<div onclick="xrayShowTab('wb-xray-variables-tab', 'Variables');">Var</div>
		</div>
		
		
		<!-- ========================= LOGS ================================== -->
		<div id="wb-xray-log-tab" class="wb-xray-tab" style="display:block;">
			serio
			<table class="wb-xray-log-table" style="display:block;">
				<tbody>
				<?php foreach($logs as $log){ ?>
					
				<tr style="<?php if($log->level < 2){ echo 'background-color:red;';} ?>">
					<td><?php echo $log->getLevelName(); ?></td>
					<td><?php echo $log->modules; ?></td>
					<td class="log-message-col"><?php echo $log->message; ?></td>
				</tr>
				<?php } ?>
				</tbody>	
			</table>
			<div class="wb-xray-message">

			</div>
		</div>
		
		<!-- ========================= QUERY ================================== -->	
		<div id="wb-xray-query-tab" class="wb-xray-tab">
			<table class="wb-xray-log-table" >
				<tbody>
				<?php foreach($logs as $log){ ?>
				<tr style="<?php if($log->level < 2){ echo 'background-color:red;';} ?>">
					<td><?php echo $log->level; ?></td>
					<td><?php echo $log->modules; ?></td>
					<td class="log-message-col"><?php echo $log->message; ?></td>
				</tr>
				<?php } ?>
				</tbody>	
			</table>
			<div class="wb-xray-message">

			</div>
		</div>		
	
		

		
		<!-- ========================= VARIABLE WATCH ================================== -->	
		
		<div id="wb-xray-variables-tab" class="wb-xray-tab">
			<div>
				<h6>GET</h6>
				<div>
					<ul>
						<?php 
						printVariables($_GET);
						//echo $p;
						?>
					</ul>
				

				</div>
			</div>
			<div>
				<h6>POST</h6>
				<div>
				<?php //foreach($_POST as $var){ ?>
				<?php //echo $var; ?>
				<?php //} ?>	
				</div>
			</div>		
			<div>
				<h6 onclick="expandVar('wb-var-list-globals');">GLOBALS</h6>
				<div>
					<ul id="wb-var-list-globals" class="wb-var-tree">
						<?php 
						printVariables($GLOBALS);
						//echo '===================================';
						//print_r($GLOBALS);

						?>
					</ul>
				</div>
			</div>
			
			<div>
				<h6>SERVER</h6>
				<div>
					<ul>
						<?php 
						printVariables($_SERVER);
						
						?>
					</ul>
				</div>
			</div>
			
			<div>
				<h6>SESSION</h6>
				<div>
					<ul>
						<?php 
						printVariables($_SESSION);
						
						?>
					</ul>
				</div>
			</div>	
			
		</div>		
		
		
	</div>
	
</div>


<?php $this->endBlock(); ?>
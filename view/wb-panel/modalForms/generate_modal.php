<?php 
$table_name = '';

require_once($_SERVER['DOCUMENT_ROOT'].'/config/database_structure.php');
?>



<html>
	<head>
		<title>Generate modal</title>
		<link rel="stylesheet" type="text/css" href="/css/webbrick/wb_layout.css">
		<link rel="stylesheet" type="text/css" href="/css/webbrick/wb_components.css">
	</head>
	<body>
	<div class="wb-section">
		<div class="wb-col-md-2"> </div>
		<div class="wb-col-md-8">
		<form action="/wb-panel/forms/GenerateModalCustomize" method="post">
			<input type="hidden" value="generate" name="action">
			<select name="table_name">
				<?php foreach( $database_structure as $tblName=>$tblVal ){ ?>
					<option value="<?php echo $tblName; ?>"><?php echo $tblName; ?></option>
				<?php } ?>
			</select>
			
			
					<div class="wb-row">
						<div class="wb-input wb-col-md-12">
							<label>Form ID</label>
							<input name="modal_id" type="text" value="">
							<span></span>
						</div>
					</div>
			
					<div class="wb-row">
						<div class="wb-input wb-col-md-12">
							<label>File Name</label>
							<input name="file_name" type="text" value="">
							<span></span>
						</div>
					</div>

			<input type="submit" value="Generate">
		</form>
			</div>
		<div class="wb-col-md-2"> </div>
	</div>		
	</body>	
</html>

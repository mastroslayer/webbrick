<?php 
$table_name = '';

require_once($_SERVER['DOCUMENT_ROOT'].'/config/database_structure.php');

if( isset($_POST['table_name']) ){
	$table_name = $_POST['table_name'];
}else{
	$table_name = null;
}

?>


<html>
	<head>
		<title>Generate modal</title>
		<link rel="stylesheet" type="text/css" href="/css/webbrick/wb_layout.css">
		<link rel="stylesheet" type="text/css" href="/css/webbrick/wb_components.css">
		<style>
			.wb-section{
				border-bottom: 1px solid grey;
				width: 800px;
				margin-left: auto;
				margin-right: auto;
			}
		</style>
	</head>
	<body>
	<form id="frm" action="/wb-panel/forms/GenerateModalFile" method="post" target="_blank">	
<?php
	echo $table_name;	
	$tbl_structure = $database_structure[$table_name]['field'];
	$i = 0;	
	foreach( $tbl_structure as $row ){
?>				
<section class="wb-section">
		<div class="wb-row" style="vertical-align:bottom;">
			<input name="<?php echo 'name['.$i.']'; ?>" type="hidden" value="<?php echo $row['name']; ?>">

			<div class="wb-input wb-col-md-4">
				<label>Etichetta</label>
				<input name="<?php echo 'label['.$i.']'; ?>" type="text" value="<?php echo $row['name']; ?>">
				<span></span>
			</div>
			
			<div class="wb-input wb-col-md-4">
				<div class="wb-checkbox">
				<input type="checkbox" id="chk_hidden_<?php echo $row['name']; ?>" name="<?php echo 'hidden['.$i.']'; ?>">
				<label for="chk_hidden_<?php echo $row['name']; ?>"></label>
				<p>hidden</p>
				</div>	
			</div>
			
			<div class="wb-input wb-col-md-4">
				<div class="wb-select">
					<select name="<?php echo 'type['.$i.']'; ?>">
						<option value="text">Text</option>
						<option value="number">Number</option>
						<option value="hidden">Hidden</option>
					</select>
				</div>	
			</div>
		</div>
</section>	
<?php $i++; } ?>	






		
			<input type="submit" value="Generate">

		</form>
					<button onclick="preview(); ">Preview</button>
		<script>
		function preview(){
			var frm = document.getElementById('frm');
			frm.action = '/wb-panel/forms/GenerateModalPreview';
			frm.submit();
		}
		</script>
	</body>	
</html>

<?php 
$this->startBlock('body');
$table_name = '';
$database_structure = $this->getData('database_structure');
//require_once($_SERVER['DOCUMENT_ROOT'].'/config/database_structure.php');

?>



	<div class="wb-section">
		<div class="wb-col-md-2"> </div>
		<div class="wb-col-md-8">
		<form action="/wb-panel/forms/GenerateModalCustomize" method="post">
			<input type="hidden" value="generate" name="action">
			
			<div class="wb-input">
				<label>Tabella</label>
				<select name="table_name">
					<?php foreach( $database_structure as $tblName=>$tblVal ){ ?>
						<option value="<?php echo $tblName; ?>"><?php echo $tblName; ?></option>
					<?php } ?>
				</select>
			</div>
			
					<div class="wb-row">
						<div class="wb-input wb-col-md-12">
							<label>Form ID</label>
							<input name="modal_id" type="text" value="">
							<span></span>
						</div>
					</div>
			
					<div class="wb-row">
						<div class="wb-input wb-col-md-12">
							<label>File Name</label>
							<input name="file_name" type="text" value="">
							<span></span>
						</div>
					</div>

			<input type="submit" value="Generate">
		</form>
			</div>
		<div class="wb-col-md-2"> </div>
	</div>		


<?php $this->endBlock(); ?>
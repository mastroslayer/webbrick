<html>

	<head>
		<title>Disinstallazione</title>
		<style>
			.card{
				width: 800px;
				height: 450px;
				text-align: center;
				border: 1px solid grey;
				margin: auto;
			}
			body{
				text-align: center;
				margin: auto;
			}
			button{
				padding: 16px;
				border: 1px solid grey;
				
			}
			
			.circle{
				    display: inline-block;
    border: 1px solid grey;
    padding: 16px;
    border-radius: 30px;
    width: 20;
    height: 20;
			}
			
			.success{
				background-color: greenyellow;
			}
			
			.error{
				background-color: red;
			}
			
			.step{
				
			}

			.step > p{
				display: inline-block;
			}
		</style>
		<script>
		var response_status = false;
		var response = null;
			
		window.onload = function(){
    		// code goes here
			start_installation();
		};	
			
		function start_installation(){
			// Create db
			execute('/setup/create_db', 'step1');
			execute('/setup/create_db', 'step2');
			
		}
			
	function execute( url, el ){
		var xhttp = new XMLHttpRequest();
		xhttp.element = el;
		//xhttp.self = self;
		xhttp.onreadystatechange = function( ) {
			if (this.readyState == 4 && this.status == 200) {
				response = this.responseText;
				//response_status = true;
				if(response == 'success'){
					document.getElementById(this.element).style.backgroundColor='green';
					response_status = true;
				}else{
					document.getElementById(this.element).style.backgroundColor='red';
					response_status = false;
				}
			}
		};
		xhttp.open("GET", url , true);
		xhttp.send();
	}	
		</script>
	</head>
	<body>
	
		<div class="card">
			
			<h3>Uninstall</h3>
			<?php if(isset($_POST['remove_db']))
			
			?>
			<div id="step1" class="step">	
				<div class="circle">1</div>
				<p>Creazione database</p>
			</div>
				
			<div class="step">	
				<div class="circle">2</div>
				<p>Creazione directory</p>
			</div>
			
			
		</div>
	</body>
</html>
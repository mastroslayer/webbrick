<html>

	<head>
		<title>Uninstall</title>
		<style>
			.card{
				width: 800px;
				height: 450px;
				text-align: center;
				border: 1px solid grey;
				margin: auto;
			}
			body{
				text-align: center;
				margin: auto;
			}
			button{
				padding: 16px;
				border: 1px solid grey;
				
			}
			
			/* The container */
.container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
	width: 300px;
    margin-left: auto;
    margin-right: auto;
	text-align: left;
}

/* Hide the browser's default checkbox */
.container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
}

/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
    background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}

	</head>
	<body>
	
		<div class="card">
			<form action="/setup/uninstall/status" method="post">
			<h3>Uninstall</h3>
			
				
			<label class="container">Elimina database
			  <input type="checkbox" checked="checked" name="delete_db">
			  <span class="checkmark"></span>
			</label>
				
			<label class="container">Elimina data directory
			  <input type="checkbox" name="delete_data">
			  <span class="checkmark"></span>
			</label>					
	
			
			<button type="submit">UNINSTALL</button>	
			
			</form>
		</div>
	</body>
</html>
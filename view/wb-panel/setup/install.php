<?php $this->startBlock('body'); ?>
		<style>
			.card{
				width: 800px;
				height: 450px;
				text-align: center;
				border: 1px solid grey;
				margin: auto;
			}
			body{
				text-align: center;
				margin: auto;
			}
			button{
				padding: 16px;
				border: 1px solid grey;
				
			}
			
			.circle{
				    display: inline-block;
    border: 1px solid grey;
    padding: 16px;
    border-radius: 30px;
    width: 20;
    height: 20;
			}
			
			.success{
				background-color: greenyellow;
			}
			
			.error{
				background-color: red;
			}
			
			.step{
				
			}

			.step > p{
				display: inline-block;
			}
			
			.loader {
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 30px;
    height: 30px;
    animation: spin 2s linear infinite;
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}
		</style>


	
		<div class="card">
			
			<h3>Install</h3>
			
			<div id="step1" class="step">	
				<div class="circle">1</div>
				<p>Creazione database</p>
			</div>
				
			<div id="step2" class="step">	
				<div class="circle">2</div>
				<p>Creazione directory</p>
			</div>
			
			<button onclick="start_installation();">Installa</button>
		</div>
				<script>
		//var response_status = false;
		//var response = null;
			
		/*window.onload = function(){
    		// code goes here
			start_installation();
		};*/	
			
		function start_installation(){
			taskCreateDb.execute();
			taskCreateDataDir.execute();
		}
		
		function TaskChain(){
			var self = this; 
			var nextNode = null; //prossima task Chain
			var preFunction = null; //funzione da eseguire prima
			var fn = null;
			var url = null;
			this.execute = function ( onSuccess, onError ){
				if( this.preFunction  != null ){
					this.preFunction();
				}
				var xhttp = new XMLHttpRequest();
				xhttp.fn = this.fn;
				xhttp.nextNode = this.nextNode;
				xhttp.onreadystatechange = function( ) {
					if (this.readyState == 4 && this.status == 200) {
						this.fn( this.responseText );
						if( this.nextNode != null){
							this.nextNode.execute();
						}
					}
				};
				xhttp.open("GET", this.url , true);
				xhttp.send();
			}
			
		}
		
					
		var taskCreateDb = new TaskChain();
		taskCreateDb.url = '/setup/install/create_db';
		taskCreateDb.preFunction = function(){
			document.getElementById('step1').getElementsByClassName('circle')[0].classList.add('loader');
		}			
		taskCreateDb.fn = function( response ){
			document.getElementById('step1').getElementsByClassName('circle')[0].classList.remove('loader');
			if(response == 'success'){
				document.getElementById('step1').style.backgroundColor='green';
			}else{
				document.getElementById('step1').style.backgroundColor='red';
			}
		}

		var taskCreateDataDir = new TaskChain();
		taskCreateDataDir.url = '/setup/install/create_data';
		taskCreateDataDir.preFunction = function(){
			document.getElementById('step2').getElementsByClassName('circle')[0].classList.add('loader');
		}		
		taskCreateDataDir.fn = function( response ){
			if(response == 'success'){
				document.getElementById('step2').getElementsByClassName('circle')[0].classList.remove('loader');
				document.getElementById('step2').style.backgroundColor='green';
			}else{
				document.getElementById('step2').style.backgroundColor='red';
			}
		}
		taskCreateDb.nextNode = taskCreateDataDir;
	/*				
	function execute( url, el ){
		//var response_status = false;
		//var response = null;
		var xhttp = new XMLHttpRequest();
		xhttp.element = el;
		//xhttp.self = self;
		xhttp.onreadystatechange = function( ) {
			if (this.readyState == 4 && this.status == 200) {
				var response = this.responseText;
				var response_status = true;
				if(response == 'success'){
					document.getElementById(this.element).style.backgroundColor='green';
					//response_status = true;
				}else{
					document.getElementById(this.element).style.backgroundColor='red';
					//response_status = false;
				}
			}
		};
		xhttp.open("GET", url , true);
		xhttp.send();
	}	*/
		</script>
<?php $this->endBlock(); ?>
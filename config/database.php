<?php

// Configurazione database

$CONF_DB_HOST = '127.0.0.1';
$CONF_DB_NAME = 'db_dev_gestionale';
$CONF_DB_TYPE = 'mysql';
$CONF_DB_USERNAME = 'dev_gestionale';
$CONF_DB_PASSWORD = 'dev_gestionale';


$CONF_DB_STRING = $CONF_DB_TYPE.':host='.$CONF_DB_HOST.';dbname='.$CONF_DB_NAME;
require_once($_SERVER['DOCUMENT_ROOT'].'/php/wb_database/WB_Database.php');

$database = new WB_Database(
	$CONF_DB_TYPE,
	$CONF_DB_HOST,
	$CONF_DB_NAME,
	$CONF_DB_USERNAME,
	$CONF_DB_PASSWORD);
require_once($_SERVER['DOCUMENT_ROOT'].'/config/database_structure.php');
$database->setStructure( $database_structure );

?>

<?php

/* -----------------------------------------------------------

------------------------------------------------------------*/
$database_structure = array(


'tbl_anagrafica'=>array(
	'field'=>array(
	array('name'=>'id_tbl_anagrafica', 'type'=>'INT', 'primary_key'=>true, 'auto_increment'=>true),

	array('name'=>'flag_ragione_sociale', 'type'=>'VARCHAR', 'size'=>'1', 'not_null'=>true, 'default'=>'0' ),

	array('name'=>'ragione_sociale', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
	array('name'=>'partita_iva', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
    array('name'=>'codice_fiscale', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
    array('name'=>'nome', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
	array('name'=>'cognome', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),

    array('name'=>'citta', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
	array('name'=>'cap', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
	array('name'=>'provincia', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
	array('name'=>'indirizzo', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
	array('name'=>'civico', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
	array('name'=>'stato', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),

    array('name'=>'email', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
    array('name'=>'telefono_ufficio', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
    array('name'=>'telefono_casa', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),


	),

	'primary_key'=>array( 'pk_tbl_anagrafica'=>'id_tbl_anagrafica')
),


'tbl_anagrafica_recapiti'=>array(
	'field'=>array(
	array('name'=>'id_tbl_anagrafica_recapiti', 'type'=>'INT', 'primary_key'=>true, 'auto_increment'=>true),
	array('name'=>'id_anagrafica', 'type'=>'INT', 'not_null'=>true ),
	array('name'=>'nome_recapito', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
	array('name'=>'citta', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
	array('name'=>'cap', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
	array('name'=>'provincia', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
	array('name'=>'indirizzo', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
	array('name'=>'civico', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
	array('name'=>'stato', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),

	),

	'primary_key'=>array( 'pk_anagrafica_recapiti'=>'id_tbl_anagrafica_recapiti'),
	'foreign_keys'=>array(
		array('name'=>'fk_TblAnagraficaRecapiti_TblAnagrafica',
			  'referenced_table'=>'tbl_anagrafica',
			  'referenced_column'=>'id_tbl_anagrafica',
			  'column'=>'id_anagrafica',
			  'on_delete'=>'cascade',
			 'on_update'=>'cascade'
			 )
	)
),


'tbl_users'=>array(
	'field'=>array(
	array('name'=>'id_tbl_users', 'type'=>'INT', 'primary_key'=>true, 'auto_increment'=>true),

	array('name'=>'username', 'type'=>'VARCHAR', 'size'=>'1', 'not_null'=>true, 'default'=>'0' ),

	array('name'=>'password', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),
	array('name'=>'email', 'type'=>'VARCHAR', 'size'=>'255', 'not_null'=>false ),



	),

	'primary_key'=>array( 'pk_tbl_users'=>'id_tbl_users')
),	


);//fine
?>

<?php


class defaultModel{

	public $database = null;

	public function __construct($database) {
		$this->database = $database;
		$this->database->connect();

	}



	/* ----------------------------------------------------------------------------------------
	 * Utility
	 *
	*/
	public function search_file( $name, $extension, $default){

        foreach( $extension as $ext ){
            $n = $name.'.'.$ext;
            //echo 'cerco: '.$n.'<br>';
            if( file_exists($_SERVER['DOCUMENT_ROOT'].$n) ){
                //echo 'found<br>';
                return $n;
            }
        }

        return $default;

    }


	/* In rimozione*/
	public function removeFile( $path, $file){
		if( file_exists($_SERVER['DOCUMENT_ROOT'].$path.$file) ){
			//echo 'elimino '.$_SERVER['DOCUMENT_ROOT'].$path.$file;
			unlink($_SERVER['DOCUMENT_ROOT'].$path.$file);
		}
	}


	public function deleteFile( $path, $file){
		if( file_exists($_SERVER['DOCUMENT_ROOT'].$path.$file) ){
			//echo 'elimino '.$_SERVER['DOCUMENT_ROOT'].$path.$file;
			unlink($_SERVER['DOCUMENT_ROOT'].$path.$file);
		}
	}


	public function deleteDir( $dir ) {
	   $files = array_diff(scandir($dir), array('.','..'));
		foreach ($files as $file) {
			if(is_dir("$dir/$file")){
			  $this->deleteDir("$dir/$file");
			}else{
			  unlink("$dir/$file");
			}
		}
		if( is_dir($dir) ){
			rmdir($dir);
			return true;
		}else{
			return false;
		}
		//return
	 }


	public function createDir($dir, $create_tree) {

	 }

	public function getFilesByName( $name, $type, $append){

	}





	public function getFiles( $dirPath ){
		$img_list = null;

		if( is_dir($dirPath) ){
			$img_list = scandir($dirPath);
			array_splice($img_list, 0, 2);
		}
		return $img_list;
	}


	/*
	 * Cerca tutti i file con una determinata estensione
	 * @param type - array - estensione file da cercare senza il .
	 * @param path - string - percorso cartella dove effettuare la ricerca
	 * @return - restituisce un array con i nomi dei file senza il percorso
	*/
	public function getFilesByType( $path, $type=null, $append=null){
			$galleryDir = $path;
			$result = array();
			$img_list = null;

			if(is_dir($galleryDir)){

				$img_list = scandir($galleryDir);
				//print_r($img_list);
				array_splice($img_list, 0, 2);
			}
			//echo 'ok';
			if(count($img_list) > 0){
				//echo 'ok2';
				foreach ($img_list as $img){
					foreach ($type as $t){
						//echo 'comparo:'.$img.' con '.$t.'<br>';
						if(pathinfo($img,  PATHINFO_EXTENSION) == $t){
							if($append != null){ $result[] = $append.$img; }
							else{$result[] = $img;}

						}
					}

				}
			}
			return $result;
	}




/*
 * Utility database
*/
	public function viewTable( $table ){
		$query_str = "SELECT * FROM ".$table;
		$iterator = $this->database->query($query_str);
		return $iterator;
	}

	public function getTableFields($table){
		$field = $this->database->get_table($table)->getFields();
		return $field;
	}


	public function selectRecord($table, $id){
		//Genera stringa aggiornamento sql
		$iterator = $this->database->selectRecord($table, $id);
		return $iterator;
	}


	public function updateRecord($table, $parametri){

		//Genera stringa aggiornamento sql
		$this->database->updateRecord($table, $parametri);
	}

	public function insertRecord($table, $parametri){
		//Genera stringa inserimento sql
		return $this->database->insertRecord($table, $parametri);
	}

	public function deleteRecord($table, $id){
		$this->database->deleteRecord($table, $id);
	}

	public function getDefaultValues( $table ){
		return $this->database->getDefaultValues( $table );

	}

}
?>

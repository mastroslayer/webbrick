<?php
try{
	require_once($_SERVER['DOCUMENT_ROOT'].'/config/global.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/php/wb_framework/WB_Kernel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/php/wb_framework/WB_Stopwatch.php');



	$wb_kernel = new WB_Kernel();

	$wb_kernel->setFrameworkPath('/php/wb_framework/');

	$wb_kernel->setExceptionController('/controller/exceptionController.php');

	$wb_kernel->loadClass('/php/wb_framework/WB_Composer.php');
	$wb_kernel->loadClass('/php/wb_database/WB_DataRow.php');
	$wb_kernel->loadClass('/php/wb_database/WB_DataSet.php');

	$wb_kernel->getRouter()->loadRoutesFromDir( $WB_ROUTER_ROUTE_PATH );


	$wb_kernel->run();
}
catch(Exception $e){
		echo '<div style="width: fit-content; 
		margin: auto; text-align: center; 
		border: 1px solid lightgrey; 
		border-radius: 5px; padding: 16px;">';
		echo '<p> WEBBRICK KERNEL ERROR </p>';
		echo '<p> CANNOT INITIALIZE KERNEL</p>';
		echo '<p>'.$e->getMEssage().'</p>';
		echo '</div>';

}
finally{	
	die();	
}







?>

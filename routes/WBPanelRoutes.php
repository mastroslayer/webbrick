<?php

$WBPanelRouteCollection = new WB_RouteCollection();
$WBPanelRouteCollection->setController('/controller/WBPanelController.php', 'WBPanelController');


$WBPanelRouteCollection->add( new WB_Route('/wb-panel',
	array('action'=>'index')
));



$WBPanelRouteCollection->add( new WB_Route('/wb-panel/forms/GenerateModal',
	array('action'=>'generate_modal')
));

$WBPanelRouteCollection->add( new WB_Route('/wb-panel/forms/GenerateModalCustomize',
	array('action'=>'generate_modal_customize'),
		array(
		'forms'=>array( 'validate'=>'[0-9]+', 'type'=>'get')
	)
));

$WBPanelRouteCollection->add( new WB_Route('/wb-panel/forms/GenerateModalFile',
	array('action'=>'generate_modal_file')
));

$WBPanelRouteCollection->add( new WB_Route('/wb-panel/forms/GenerateModalPreview',
	array('action'=>'generate_modal_preview')
));

/* Setup routes */
$WBPanelRouteCollection->add( new WB_Route('/wb-panel/routes/view-routes',
	array('action'=>'routesList')
));

/*
$route = new WB_Route('/wb-panel/routes/view-routes');
$router->controllerPath = '/Path/To/My/Controller';
$route->controller = 'myController';
$route->action = 'myAction';
$route->validate('view-routes', '[0-9]', );
$route->setParameter('view-routes', 'get');
$WBPanelRouteCollection->add( $route );

$route->setAction('myaction', array($parameter));


*/

$WBPanelRouteCollection->add( new WB_Route('/wb-panel/routes/save',
	array('action'=>'routesSave')
));


$WBPanelRouteCollection->add( new WB_Route('/wb-panel/logs',
	array('action'=>'logs')
));




/* Setup routes*/

$WBPanelRouteCollection->add( new WB_Route('/wb-panel/setup/install',
	array('action'=>'install')
));

$WBPanelRouteCollection->add( new WB_Route('/wb-panel/setup/uninstall',
	array('action'=>'uninstall')
));

$WBPanelRouteCollection->add( new WB_Route('/wb-panel/setup/uninstall/status',
	array('action'=>'uninstall_status')
));

$WBPanelRouteCollection->add( new WB_Route('/wb-panel/setup/uninstall/remove_db',
	array('action'=>'uninstall_remove_db')
));

$WBPanelRouteCollection->add( new WB_Route('/wb-panel/setup/uninstall/remove_db',
	array('action'=>'uninstall_remove_data')
));

$WBPanelRouteCollection->add( new WB_Route('/wb-panel/version_php',
	array('action'=>'version_php')
));



$WBPanelRouteCollection->add( new WB_Route('/wb-panel/database/tables',
	array('action'=>'tableList')
));

$WBPanelRouteCollection->add( new WB_Route('/wb-panel/database/table',
	array('action'=>'tableDetail')
));
/*
$WBPanelRouteCollection->add( new WB_Route('/wb-panel',
	array('action'=>'prodotti')
));


$setupRouteCollection->add( new WB_Route('/setup',
	array('action'=>'index')
));

$setupRouteCollection->add( new WB_Route('/setup/install',
	array('action'=>'install')
));

$setupRouteCollection->add( new WB_Route('/setup/install/create_db',
	array('action'=>'install_create_db')
));

$setupRouteCollection->add( new WB_Route('/setup/install/create_data',
	array('action'=>'install_create_data')
));



$WBPanelRouteCollection->add( new WB_Route('/prodotti/importSettings.html',
	array('action'=>'import_settings')
));


$WBPanelRouteCollection->add( new WB_Route('/prodotti/importListino.html',
	array('action'=>'import2')
));

$WBPanelRouteCollection->add( new WB_Route('/import.html',
	array('action'=>'import')
));
*/

$this->addRouteCollection($WBPanelRouteCollection);










?>

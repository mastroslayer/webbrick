<?php 
$errorRoutes = new WB_RouteCollection();
$routeCollectionName->add(new WB_Route('/wb-panel',
	array(action=>'index'),
),
)
$routeCollectionName->add(new WB_Route('/wb-panel/forms/GenerateModal',
	array(action=>'generate_modal'),
),
)
$routeCollectionName->add(new WB_Route('/wb-panel/forms/GenerateModalCustomize',
	array(action=>'generate_modal_customize'),
	array(	'name'=>array('name'=>'forms','exp'=>'[0-9]+','exp_start'=>'/\b','exp_end'=>'\b/','type'=>'get',),
)
$routeCollectionName->add(new WB_Route('/wb-panel/forms/GenerateModalFile',
	array(action=>'generate_modal_file'),
),
)
$routeCollectionName->add(new WB_Route('/wb-panel/forms/GenerateModalPreview',
	array(action=>'generate_modal_preview'),
),
)
$routeCollectionName->add(new WB_Route('/wb-panel/routes/view-routes',
	array(action=>'routesList'),
),
)
$routeCollectionName->add(new WB_Route('/wb-panel/routes/save',
	array(action=>'routesSave'),
),
)
$routeCollectionName->add(new WB_Route('/wb-panel/logs',
	array(action=>'logs'),
),
)
$routeCollectionName->add(new WB_Route('/wb-panel/setup/install',
	array(action=>'install'),
),
)
$routeCollectionName->add(new WB_Route('/wb-panel/setup/uninstall',
	array(action=>'uninstall'),
),
)
$routeCollectionName->add(new WB_Route('/wb-panel/setup/uninstall/status',
	array(action=>'uninstall_status'),
),
)
$routeCollectionName->add(new WB_Route('/wb-panel/setup/uninstall/remove_db',
	array(action=>'uninstall_remove_db'),
),
)
$routeCollectionName->add(new WB_Route('/wb-panel/setup/uninstall/remove_db',
	array(action=>'uninstall_remove_data'),
),
)
$routeCollectionName->add(new WB_Route('/wb-panel/version_php',
	array(action=>'version_php'),
),
)
$routeCollectionName->add(new WB_Route('/wb-panel/database/tables',
	array(action=>'tableList'),
),
)
$routeCollectionName->add(new WB_Route('/wb-panel/database/table',
	array(action=>'tableDetail'),
),
)
$routeCollectionName->add(new WB_Route('',
	array(action=>'index'),
),
)
$routeCollectionName->add(new WB_Route('/modal_confirm',
	array(action=>'modal_confirm'),
),
)
$routeCollectionName->add(new WB_Route('/error/error_code',
	array(action=>'error404'),
),
)
$this->addRouteCollection($errorRoutes);
?>
